/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PURE_PURSUIT_PURE_PURSUIT_CORE_H
#define PURE_PURSUIT_PURE_PURSUIT_CORE_H

// ROS includes
// #include <geometry_msgs/PoseStamped.h>
// #include <geometry_msgs/TwistStamped.h>
#include <ros/ros.h>
#include <std_msgs/Float32.h>
// #include <visualization_msgs/Marker.h>

// User defined includes

#include <nmea_msgs/Sentence.h>
#include <gps_common/GPSFix.h>

#include <vector>
#include <memory>


using namespace std;

namespace gnss_driver{


class GnssDriverNmea
{
public:
  GnssDriverNmea();
  ~GnssDriverNmea();

  void run();

private:
  // handle
  ros::NodeHandle nh_;
  ros::NodeHandle private_nh_;

  // class
  // PurePursuit pp_;
  gps_common::GPSFix gps_fix_;

  // publisher
  ros::Publisher pub_gnss_fix_;

  // subscriber
  ros::Subscriber sub_nmea_ ;

  // constant
  const int LOOP_RATE_;  // processing frequency


  // string gps_way_point_file;

  bool flag_stop;
  bool is_first_run;
  bool is_gpgga_update; 
  bool is_heading_update; 

  // initializer
  void initForROS();

  // callbacks
  void callbackNmeaMsgs( const nmea_msgs::Sentence& msg);


};

// inline double kmph2mps(double velocity_kmph)
// {
//   return (velocity_kmph * 1000) / (60 * 60);
// }

}  // namespace 

#endif  // PURE_PURSUIT_PURE_PURSUIT_CORE_H
