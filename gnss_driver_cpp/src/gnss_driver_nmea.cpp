/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vector>
#include <gnss_driver/gnss_driver_nmea.h>
#include <boost/algorithm/string.hpp>


namespace gnss_driver
{
// Constructor
GnssDriverNmea::GnssDriverNmea()
  : private_nh_("~")
  , LOOP_RATE_( 1000)
  //, is_pose_set_(false)
  //, command_linear_velocity_(0)
{
  initForROS();
}

// Destructor
GnssDriverNmea::~GnssDriverNmea() {}

void GnssDriverNmea::initForROS()
{
  // ros parameter settings

  // private_nh_.param<std::string>( "gps_way_point_file", gps_way_point_file, ""); // gps 路点
  // private_nh_.param( "velocity_source", velocity_source_, 0);
  // private_nh_.param( "is_linear_interpolation", is_linear_interpolation_, true);

  sub_nmea_ = nh_.subscribe("/nmea_sentence", 10, &GnssDriverNmea::callbackNmeaMsgs, this);
  pub_gnss_fix_ = nh_.advertise<gps_common::GPSFix>("/gnss/fix", 10);
}


void GnssDriverNmea::callbackNmeaMsgs( const nmea_msgs::Sentence& msg)
{
    // ROS_INFO_STREAM(msg.sentence);
    if (msg.sentence.size() < 1){
      return;
    }
   
    std::vector<std::string> gpsVec;
    // ips="192.168.1.4;192.168.1.5;192.168.1.6;";
    boost::split(gpsVec, msg.sentence, boost::is_any_of(","));
    if (gpsVec.size() < 2) {
      // RCLCPP_ERROR(rclcpp::get_logger("tag_serial_driver"), "gps message spilt error : %ld !!!", gpsVec.size());
      ROS_ERROR("msg.sentence size < 2   ");
      return;
    }

    // for(auto ss : gpsVec){
    //   ROS_INFO_STREAM(ss);
    // }

    /*******************************
        0 :  $GPGGA
        1 :  083121.50
        2 :  3642.22989349
        3 :  N
        4 :  11708.94388080
        5 :  E
        6 :  5
        7 :  17
        8 :  1.1
        9 :  51.9280
        10 :  M
        11 :  -6.6466
        12 :  M
        13 :  01
        14 :  2594*4B
    */
    if (gpsVec[0] == "$GPGGA" || gpsVec[0] == "$GNGGA") {
      // for(uint i = 0; i < gpsVec.size(); i++){
      //   std::cout << i << " :  "  << gpsVec[i] << std::endl;
      // }

      // lon
      if (gpsVec[4].size() > 0){
        int lon_dd = stoi(gpsVec[4])/100;
        double lon_mm = (stod(gpsVec[4]) - lon_dd * 100 ) /60.0;
        gps_fix_.longitude  =  lon_dd + lon_mm;
      }

      // lat
      if (gpsVec[2].size() > 0){
        int lat_dd = stoi(gpsVec[2]) /100;
        double lat_mm = (stod(gpsVec[2]) - lat_dd * 100 ) /60.0;
        gps_fix_.latitude  =  lat_dd + lat_mm;
      }

      // status
      int status =  0;
      if (gpsVec[6].size() > 0){

        // std::cout <<  "status :  "  << gpsVec[6] << std::endl;
        status =  stoi(gpsVec[6]);
      }
      switch(status){
        case 4:
          gps_fix_.status.status  = gps_fix_.status.STATUS_FIX; 
          break;
        case 5:
          gps_fix_.status.status  = gps_fix_.status.STATUS_WAAS_FIX; 
          break;
        default:
          gps_fix_.status.status  = gps_fix_.status.STATUS_NO_FIX; 
          break;
      }
    }

    /***************************
      0 :  $GNVTG
      1 :  109.960
      2 :  T
      3 :  116.478
      4 :  M
      5 :  0.00756
      6 :  N
      7 :  0.01400
      8 :  K
      9 :  D*33
      */
    if (gpsVec[0] == "$GPVTG" || gpsVec[0] == "$GNVTG") {
      // for(uint i = 0; i < gpsVec.size(); i++){
      //   //  std::cout << i << " :  "  << gpsVec[i] << std::endl;
      // }

      if (gpsVec[7].size() > 0){
        gps_fix_.speed  =  stod(gpsVec[7]) / 3.6;
      }

    }

      /*********************************
      0 :  #HEADINGA
      1 :  COM2
      2 :  0
      3 :  41.0
      4 :  FINE
      5 :  2232
      6 :  291989.450
      7 :  11749138
      8 :  21
      9 :  18;SOL_COMPUTED
      10 :  NARROW_INT
      11 :  1.2017
      12 :  83.9285
      13 :  5.2470
      14 :  0.0000
      15 :  0.5560
      16 :  1.6978
      17 :  "999"
      18 :  23
      19 :  16
      20 :  16
      21 :  6
      22 :  3
      23 :  00
      24 :  1
      25 :  d3*64bb6e2a
        ****/

    if (gpsVec[0] == "#HEADINGA") {
      //for(uint i = 0; i < gpsVec.size(); i++){
      //  std::cout << i << " :  "  << gpsVec[i] << std::endl;
      //}

      //  ROS_INFO_STREAM(ss);
        if (gpsVec[12].size() > 0){
          gps_fix_.track  =  stod(gpsVec[12]);

          pub_gnss_fix_.publish(gps_fix_);
        }

    }

          // fix.status.status = fix.status.STATUS_FIX
          // fix.latitude = self.__info.get('lat',0)
          // fix.longitude = self.__info.get('lon',0)
          // fix.altitude = self.__info.get('h',0)
          // fix.track = self.__info.get('azimutha',0)
          // fix.speed = self._odometry.twist.twist.linear.x  # 地面速度
          // fix.climb = self._odometry.twist.twist.linear.z  # 垂直速度
          // fix.dip   = self.__info.get('yaw',0)
          // fix.pitch = self.__info.get('pitch',0)
          // fix.roll  = self.__info.get('roll',0)


}

void GnssDriverNmea::run()
{
  ROS_INFO_STREAM("gnss_driver_nmea_core start");
  ros::Rate loop_rate(LOOP_RATE_);


  while (ros::ok())
  {

    try {
        ros::spinOnce();

            // //  发送topic
            // pm_waypoint_follower_msgs::PureSursuit_Info pure_info;
            // pure_info.header.stamp = ros::Time::now();
            // pure_info.min_distance = min_distance ;
            // // pure_info.status  = pure_info.RUNING;
            // pub1_.publish(pure_info);
    }catch (const exception& e){
            cerr    << "Caught exception: " << e.what() << "\n";
            // qDebug() << "Caught exception: " << e.what() << "\n";
    }
    loop_rate.sleep();
    //===========================================================//
    // ROS_INFO("-->\n");
  }
}
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "GnssDriverNmea");
  gnss_driver::GnssDriverNmea ppn;
  ppn.run();

  return 0;
}