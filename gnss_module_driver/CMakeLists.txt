cmake_minimum_required(VERSION 2.8.3)
project(gnss_module_driver)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED )

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)


## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
catkin_python_setup()

catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES ros_pyqt
#  CATKIN_DEPENDS other_catkin_pkg
#  DEPENDS system_lib
)

catkin_install_python(
  PROGRAMS  
    scripts/gnss_carla_driver
    scripts/gnss_lgsvl_driver
    scripts/gnss_nmea_driver
    scripts/gnss_ins_driver
    scripts/gnss_NewtonM2_driver
    scripts/gnss_P2_driver
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)


install(DIRECTORY launch/
   DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/launch
   FILES_MATCHING PATTERN "*.launch"
)

# install(DIRECTORY ui/
#    DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/ui
#   #  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}/ui
#    FILES_MATCHING PATTERN "*.ui"
# )


