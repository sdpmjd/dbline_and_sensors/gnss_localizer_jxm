#!/usr/bin/env python2
#coding=utf-8

import logging

class GpsPares_NewtonM2(object):
    """
    ['$GPFPD', '2117', '546559.380', '81.348', '1.461', 
      '0.295', '36.70388968', '117.14939980', '38.02', '0.005', 
      '0.001', '-0.002', '1.366', '22', '24', 
      '4B*2B']
    ["$GTIMU,2133,456124.100,-0.0634,0.7186,0.3897,-0.0228,0.0011,0.9868,31.5*60"]

    """
    def parse( self,msg ):
        info = {}
        tmp_list = msg.split(",")
        # print(tmp_list)
        try:
            if tmp_list[0] in ["$GPFPD"]:
                if tmp_list[3] == "":
                    print("GpsPares_NewtonM2  parse error !!!")
                    return {}

                info['heading'] = float(tmp_list[3])  # 航向角
                # info['yaw'] = rotate_deg.heading2yaw(info['heading']) 
                info['pitch'] = float(tmp_list[4])
                info['roll'] = float(tmp_list[5])

                info['lat'] = float(tmp_list[6])
                info['lon'] = float(tmp_list[7])
                info['altitude'] = float(tmp_list[8])

                Ve = float(tmp_list[9]) # 东向速度 m/s
                Vn = float(tmp_list[10]) # 北向速度 m/s
                Vu = float(tmp_list[11]) # 天向速度 m/s 

                info['v'] = ((Ve**2 +Vn**2)**0.5)* 3.6
                # info['cnt'] = int(tmp_list[13][0:2])
                info['cnt'] = int(tmp_list[13]) + int(tmp_list[14])
                info["gps_state"] = 0  
                if tmp_list[15][0:2] == "4B":
                    info["gps_state"] = 4  

                return info

            if tmp_list[0] in ["$GTIMU"]:
                info["gyro_x"] = float(tmp_list[3]) 
                info["gyro_y"] = float(tmp_list[4]) 
                info["gyro_z"] = float(tmp_list[5]) 

                _acc_x = float(tmp_list[6]) * 9.8
                _acc_y = float(tmp_list[7]) * 9.8
                _acc_z = float(tmp_list[8]) * 9.8 

                info['acc_x'] = _acc_x
                info['acc_y'] = _acc_y
                info['acc_z'] = _acc_z 

                return info
        except Exception as e:
            print(e)
            return {}

# import math
# import struct
# RAD2DEG = 180/math.pi

def log_init():
    from utils.loginit import init_logger
    import platform
    if platform.system() == "Windows":
        init_logger("C:\\Users\\Administrator\Desktop\\gps.log")
    elif platform.system() == "Linux":
        init_logger("/tmp/gps.log")
    elif platform.system() == "Darwin":
        init_logger("/tmp/gps.log")
    else:
        init_logger()
def test_main():
    gps_pares = GpsPares_NewtonM2()
    msg = "$GPFPD,2117,546559.380,81.348,1.461,0.295,36.70388968,117.14939980,38.02,0.005,0.001,-0.002,1.366,22,24,4B*2B"
    ret = gps_pares.parse(msg)
    print(ret)

    pass
if __name__ == "__main__":
    test_main()
    pass


