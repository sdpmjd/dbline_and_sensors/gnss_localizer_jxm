#!/usr/bin/env python3
#coding=utf-8

import rospy
from std_msgs.msg import String
from nmea_msgs.msg import Sentence
from gnss_ncom_driver.gnss_pares import GpsPares_NOCM

from gnss_ncom_driver import rotate_deg 

from sensor_msgs.msg import Imu
from sensor_msgs.msg import NavSatFix
from gps_common.msg import GPSFix

from geographiclib.geodesic import Geodesic

class GeodesicWGS84:
    geod = Geodesic.WGS84

    @classmethod
    def next_gps_point(cls,cur_lat,cur_lon,azi=0,len=2.2):
        '''
        :param cur_gps_point:  当前坐标 dirc
        :param azi:            航向角
        :param len:            距离
        :return:
        '''
        g = cls.geod.Direct(lat1=cur_lat, lon1=cur_lon, azi1=azi, s12=len)
        info ={}
        info['lat'] =  g['lat2']
        info['lon'] =  g['lon2']
        return info

class Node(object):
    def __init__(self,argv):
        self.init_argv = argv
        self.__info = {}

        rospy.init_node ( 'gps_module_core' , anonymous=True)

        rospy.loginfo("init")
        # gnss_info_topic_name =  rospy.get_param('~gnss_info_topic_name', 'gnss_info')

        self._input_topic_imu_raw = rospy.get_param('~input_topic_imu_raw', "/imu_raw") 
        self._input_topic_odom = rospy.get_param('~input_topic_odom', "/vehicle/odom")

        self._output_topic_imu_raw = rospy.get_param('~output_topic_imu_raw', "/gnss/imu_raw") 
        self._output_topic_odom = rospy.get_param('~output_topic_odom', "/gnss/odom")
        self._output_topic_fix = rospy.get_param('~output_topic_fix', '/gnss/fix')
        self._output_topic_navsat_fix = rospy.get_param('~output_topic_navsat_fix', '/gnss/navsat_fix')

        self.d_heading = rospy.get_param('~d_heading', 0) #<!-- 负数为逆时针旋转-->
        self.antenna_distance = rospy.get_param('~antenna_distance', 1) #<!--左边定位，右边定向-->

        self.d_yaw = rospy.get_param('~d_yaw', -90)  # default -90

        self.gps_pares =  GpsPares_NOCM() # NMEA

        rospy.Subscriber ( "/nmea_sentence", Sentence , self.Callback,queue_size=1)

        self.gnss_fix_pub = rospy.Publisher ( self._output_topic_fix , GPSFix , queue_size=1 )
        self.gnss_navsat_fix_pub = rospy.Publisher ( self._output_topic_navsat_fix , NavSatFix , queue_size=1 )
        self.gnss_imu_pub = rospy.Publisher ( self._output_topic_imu_raw , Imu , queue_size=1 )

        rospy.Timer ( rospy.Duration ( secs = 0, nsecs=50*1000000 ) , self.timer_callback )  # send to socket

    def run(self):
        rospy.spin()

    def parse( self,msg_string ):
        try:
            info = self.gps_pares.parse(msg_string)
            if info.__len__() == 0:
                return
            self.__info['lat']       = float(info.get('lat', self.__info.get('lat',0) ) ) #/100
            self.__info['lon']       = float(info.get('lon',self.__info.get('lon',0))) #/100
            if "h" in info:
                self.__info['h']       = float(info.get('h',self.__info.get('h',0))) #/100
            if 'altitude' in info:
                self.__info['h']       = float(info.get('altitude')) #/100

            if 'v' in info:
                self.__info['speed']     = float(info.get('v',  self.__info.get('v',0)))
            self.__info['gps_state'] = int(info.get('gps_state',self.__info.get('gps_state',0)))
            if 'azi_a' in info:
                self.__info['azimutha']  = float(info.get('azi_a'))
            self.__info['cnt']       = float(info.get('cnt',0))

            #  角速度
            self.__info['gyro_x']    = float(info.get('gyro_x',  0))
            self.__info['gyro_y']    = float(info.get('gyro_y',  0))
            self.__info['gyro_z']    = float(info.get('gyro_z',  0))

            #  加速度
            self.__info['acc_x']     = float(info.get('acc_x',  0))
            self.__info['acc_y']     = float(info.get('acc_y',  0))
            self.__info['acc_z']     = float(info.get('acc_z',  0))

            try:
                if self.d_heading !=0:
                    _info2 = GeodesicWGS84.next_gps_point(self.__info['lat'],self.__info['lat'],self.__info['azimutha'],self.antenna_distance/2)
                    self.__info['lat'] = _info2['lat'] 
                    self.__info['lon'] = _info2['lon'] 

                    self.__info['azimutha'] += self.d_heading
                    if self.__info['azimutha'] > 360: 
                        self.__info['azimutha'] = self.__info['azimutha'] - 360
                    if self.__info['azimutha'] < 0: 
                        self.__info['azimutha'] = 360 + self.__info['azimutha']

            except Exception as e:
                rospy.logerr(e)

            # 旋转
            _yaw =rotate_deg.heading2yaw(self.__info['azimutha'])
            self.__info['yaw'] =rotate_deg.rotate(_yaw,self.d_yaw) 

            if  "pitch" in info:
                self.__info['pitch'] = info["pitch"]
            if  "roll" in info:
                self.__info['roll'] = info["roll"]

            if self.__info['gps_state'] != 4:
                msg = "error gps_state %s " % self.__info.get('gps_state')
                rospy.logerr(msg)
            #     # logging.error("GPS state = %s " % self.gps_info['gps_state'] )
        except Exception as e:
            print(e)

    def timer_callback( self ,event):
        try:

            fix = GPSFix()
            fix.header.stamp = rospy.get_rostime()
            fix.header.frame_id = "/gps"#frame_id
            fix.status.status = fix.status.STATUS_NO_FIX
            if self.__info.get('gps_state',0) == 4:
                fix.status.status = fix.status.STATUS_FIX
            if self.__info.get('gps_state',0) == 5:
                fix.status.status = fix.status.STATUS_WAAS_FIX

            fix.latitude = self.__info.get('lat',0)
            fix.longitude = self.__info.get('lon',0)
            fix.altitude = self.__info.get('h',0)
            fix.track = self.__info.get('azimutha',0)
            fix.speed = self.__info.get('speed',0) /3.6 
            fix.climb = 0  # 垂直速度
            fix.dip   = self.__info.get('yaw',0)
            fix.pitch = self.__info.get('pitch',0)
            fix.roll  = self.__info.get('roll',0)
            self.gnss_fix_pub.publish(fix)

            ############
            navsat_fix = NavSatFix()
            navsat_fix.header.stamp = rospy.get_rostime()
            navsat_fix.header.frame_id = 'navsat_link'

            navsat_fix.status.status = navsat_fix.status.STATUS_NO_FIX
            if self.__info.get('gps_state',0) == 4:
                navsat_fix.status.status = navsat_fix.status.STATUS_FIX

            navsat_fix.latitude = self.__info.get('lat',0)
            navsat_fix.longitude = self.__info.get('lon',0)
            navsat_fix.altitude = self.__info.get('h',0)

            self.gnss_navsat_fix_pub.publish(navsat_fix)
            
        except  Exception as e:
            print(e)
        finally:
            pass

    def Callback( self,data):
        self.parse(data.sentence)

def main():
    import sys
    node = Node(sys.argv)
    node.run()

if __name__ == "__main__":
    main()