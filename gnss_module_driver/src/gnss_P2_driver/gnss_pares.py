#!/usr/bin/env python3
#coding=utf-8

import  logging
from copy import  deepcopy

# from gnss_module_driver import rotate_deg 

class GpsPares_P2(object):

    ''' 华测 P2 解析 '''

    def __init__(self):
        self.info = {}

    def parse( self,msg ):

        info = {}
        for gpchc in msg.split('$'):
            data = gpchc.split(',')
            if data.__len__() < 24:
                continue

            try:
                info['azi_a'] = data[3]
                info['pitch'] = data[4]
                info['roll']  = data[5]

                info['gyro_x'] = data[6]
                info['gyro_y'] = data[7]
                info['gyro_z'] = data[8]
                info['acc_x' ] = float(data[9])   * 9.8
                info['acc_y' ] = float(data[10])  * 9.8
                info['acc_z' ] = float(data[11]) * 9.8

                info['lat'] = 0
                info['lon'] = 0

                if data[12] != "":
                    info['lat'] = data[12]
                    info['lon'] = data[13]
                    info['h'] = data[14]

                info['v'] = float(data[18]) * 3.6
                info['cnt'] = data[19]

                info["gps_state"]  = int(data[21][0])
                info["gps_state_sys"]  = int(data[21][1])

                # info["gps_state"]  =tmp_list[21][0:1]
                # if data[21][1] == '0':
                #     info["gps_state"] = int('101')
            except Exception as e:
                print(data)
                logging.exception(e)

        return info


def log_init():
    from utils.loginit import init_logger
    import platform
    if platform.system() == "Windows":
        init_logger("C:\\Users\\Administrator\Desktop\\gps.log")
    elif platform.system() == "Linux":
        init_logger("/tmp/gps.log")
    elif platform.system() == "Darwin":
        init_logger("/tmp/gps.log")
    else:
        init_logger()
def test_main():
    gps_pares = GpsPares_P2()

    pass
if __name__ == "__main__":
    test_main()

    pass


