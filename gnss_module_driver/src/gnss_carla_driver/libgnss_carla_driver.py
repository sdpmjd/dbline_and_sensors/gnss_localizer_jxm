#!/usr/bin/env python2
#coding=utf-8

import sys
import rospy
from std_msgs.msg import String

# from gnss_module_carlar_driver.gnss_pares import GpsPares
from tf.transformations import euler_from_quaternion, quaternion_from_euler

from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from sensor_msgs.msg import NavSatFix
from gps_common.msg import GPSFix

from geometry_msgs.msg import TwistStamped

import math
RAD2DEG = 180/math.pi

class Node(object):
    def __init__(self):
        self.__info = {}
        self._odometry =  Odometry()
        rospy.init_node ( 'gps_module_core' ,log_level=rospy.INFO ,anonymous=True)

        rospy.loginfo("init")

        self._input_topic_imu_raw = rospy.get_param('~input_topic_imu_raw',"/carla/ego_vehicle/imu/imu1") 
        self._input_topic_odom = rospy.get_param('~input_topic_odom', "/carla/ego_vehicle/odometry")
        self._input_topic_navsat_fix = rospy.get_param('~input_topic_navsat_fix', "/carla/ego_vehicle/gnss/gnss1/fix")

        self._output_topic_imu_raw = rospy.get_param('~output_topic_imu_raw', "/gnss/imu_raw") 
        self._output_topic_odom = rospy.get_param('~output_topic_odom', "/gnss/odom")
        self._output_topic_fix = rospy.get_param('~output_topic_fix', '/gnss/fix')
        self._output_topic_navsat_fix = rospy.get_param('~output_topic_navsat_fix', '/gnss/navsat_fix')
        self._output_topic_gnss_twist = rospy.get_param('~output_topic_gnss_twist', '/gnss/gnss_twist')


        # /carla/ego_vehicle/gnss/gnss1/fix
        rospy.Subscriber (self._input_topic_navsat_fix,  NavSatFix, self.carla_navsatfix_callback,queue_size=1)

        rospy.Subscriber (self._input_topic_imu_raw, Imu , self.imu_callback,queue_size=1)
        rospy.Subscriber ( self._input_topic_odom, Odometry , self.odom_callback,queue_size=1)

        self.gnss_fix_pub = rospy.Publisher ( self._output_topic_fix , GPSFix , queue_size=1 )
        # self.gnss_odom_pub = rospy.Publisher ( self._output_topic_odom , Odometry , queue_size=1 )
        self.gnss_navsat_fix_pub = rospy.Publisher ( self._output_topic_navsat_fix , NavSatFix , queue_size=1 )
        self.gnss_imu_pub = rospy.Publisher ( self._output_topic_imu_raw , Imu , queue_size=1 )
        self.gnss_twist_pub = rospy.Publisher ( self._output_topic_gnss_twist , TwistStamped , queue_size=1 )

        rospy.Timer ( rospy.Duration ( secs = 0, nsecs=50*1000000 ) , self.timer_callback )  # send to socket

        self.latitude_switch_cnt = 0

    def run(self):
        #rospy.loginfo("run : thread pid %s ...." % os.getpid())
        rospy.spin()

    # def parse( self,msg_string ):
    #     try:
    #         info = self.gps_pares.parse(msg_string)
    #         if info.__len__() == 0:
    #             return
    #         # print(info)
    #         self.__info['lat']       = float(info.get('lat', self.__info.get('lat',0) ) ) 
    #         self.__info['lon']       = float(info.get('lon',self.__info.get('lon',0))) 
    #         self.__info['h']       = float(info.get('h',self.__info.get('h',0))) 
    #         # self.__info['speed']     = float(info.get('v',  self.__info.get('speed',0)))
    #         self.__info['gps_state'] = int(info.get('gps_state',self.__info.get('gps_state',0)))
    #         self.__info['azimutha']  = float(info.get('azi_a', self.__info.get('azimutha',0)))
    #         self.__info['cnt']       = float(info.get('cnt',self.__info.get('cnt',0)))


    #         self.__info['gps_state'] = 4
    #     except Exception as e:
    #         print(e)

    def timer_callback( self ,event):
        try:
            fix = GPSFix()
            fix.header.stamp = rospy.get_rostime()
            fix.header.frame_id = "/gps"#frame_id
            fix.status.status = fix.status.STATUS_FIX
            fix.latitude = self.__info.get('lat',0)

            if fix.longitude != self.__info.get('lon',0):
                fix.longitude = self.__info.get('lon',0)
            # else:
            if self.latitude_switch_cnt %2 == 0:
                fix.longitude = fix.longitude +  0.000000001
            else:
                fix.longitude = fix.longitude -  0.000000001
            self.latitude_switch_cnt += 1


            fix.altitude = self.__info.get('h',0)
            fix.track = self.__info.get('azimutha',0)
            fix.speed = self._odometry.twist.twist.linear.x  # 地面速度
            fix.climb = self._odometry.twist.twist.linear.z  # 垂直速度
            fix.dip   = self.__info.get('yaw',0)
            fix.pitch = self.__info.get('pitch',0)
            fix.roll  = self.__info.get('roll',0)

            self.gnss_fix_pub.publish(fix)

            # navsat_fix = NavSatFix()
            # navsat_fix.header.frame_id = 'navsat_link'

            # navsat_fix.status.status = navsat_fix.status.STATUS_NO_FIX
            # if self.__info.get('gps_state',0) == 4:
            #     navsat_fix.status.status = navsat_fix.status.STATUS_FIX

            # navsat_fix.latitude = self.__info.get('lat',0)
            # navsat_fix.longitude = self.__info.get('lon',0)
            # navsat_fix.altitude = self.__info.get('h',0)

            # self.gnss_navsat_fix_pub.publish(navsat_fix)

        except  Exception as e:
            print(e)
        finally:
            pass

    def carla_navsatfix_callback(self,navsat_fix):
        navsat_fix.status.status = navsat_fix.status.STATUS_FIX

        self.__info['lat'] = navsat_fix.latitude  
        self.__info['lon'] = navsat_fix.longitude  
        self.__info['h'] = navsat_fix.altitude  



    def imu_callback(self,imu): 

        # imu.angular_velocity
        imu.header.frame_id = 'imu_link'
        self.__info['gyro_x'] = imu.angular_velocity.x  # 陀螺仪 X轴角速度
        self.__info['gyro_y'] = imu.angular_velocity.y  
        self.__info['gyro_z'] = imu.angular_velocity.z 

        # imu.linear_acceleration
        self.__info['acc_x'] = imu.linear_acceleration.x 
        self.__info['acc_y'] = imu.linear_acceleration.y 
        self.__info['acc_z'] = imu.linear_acceleration.z 

        roll,pitch,yaw  = self.get_euler(imu.orientation)
        yaw_deg = yaw * RAD2DEG
        pitch_deg = pitch * RAD2DEG
        roll_deg = roll * RAD2DEG

        # print("\nroll = %.4f, pitch = %.4f, yaw = %.4f" %(roll,pitch,yaw))
        # print("roll = %.4f, pitch = %.4f, yaw = %.4f" %(roll_deg,pitch_deg,yaw_deg))
        self.__info['yaw']   = yaw_deg 
        self.__info['pitch'] = pitch_deg 
        self.__info['roll']  = roll_deg 
        _azimutha = self.__info.get('yaw',0) 

        # (-180,180)  conver to (0-360)
        if _azimutha < 0:  
            _azimutha = - self.__info['yaw'] 
        if _azimutha > 0:  
            _azimutha = 360 - self.__info['yaw'] 

        # 旋转 (90)
        if _azimutha >= 270:
            _azimutha -= 270
        else: 
            _azimutha += 90

        self.__info["azimutha"] = _azimutha 

        self.gnss_imu_pub.publish(imu)

        # /estimate_twist geometry_msgs/TwistStamped
        gnss_twist = TwistStamped()
        gnss_twist.header.stamp = rospy.get_rostime()
        gnss_twist.header.frame_id="/base_link"
        gnss_twist.twist.linear.x =  self._odometry.twist.twist.linear.x # 纵向速度
        gnss_twist.twist.angular.z = imu.angular_velocity.z  #  旋转角速度
        self.gnss_twist_pub.publish(gnss_twist)


    # 四元数转RPY欧拉角
    def get_euler (self,orientation):
        orientation_q =orientation
        orientation_list = [orientation_q.x, orientation_q.y, orientation_q.z, orientation_q.w]
        (roll, pitch,yaw ) = euler_from_quaternion (orientation_list)

        return (roll, pitch,  yaw )

    def odom_callback(self,odom):
        self.__info['speed'] = odom.twist.twist.linear.x * 3.6
        self._odometry = odom
        # self.gnss_odom_pub.publish(odom)

def main():
    node = Node()
    node.run()

if __name__ == "__main__":
    main()