#!/usr/bin/env python3
#coding=utf-8

import  logging
from copy import  deepcopy

# from gnss_module_driver import rotate_deg 

class GpsPares_INS(object):

    '''  北斗 INS 解析 '''
    '''
    AT_ITINS=<lat>,<lon>,<alt>,<height>,<utc>,<ns_vel>,<ew_vel>, 
    <gnd_spd>,<track_ angle>,<roll>,<pitch>,<heading>,<mag_heading>, 
    <roll_rate>,<pitch_rate>,<heading_rate><CR><LF>

    "AT_ITINS=36.7038,117.1486,52,38,2020.06.27-04:02:10,-1.3,-1.9,2.3,237,-0.3,0.2,233.9,0,-1.1,-0.7,-18.2"
    '''

    #5501596d00b4291d00acfffaff010001003a001300980f0000000000000000fe04ec015890e0156f87d345b9160000ffffffff00000000000000003a90e015e187d345f916000002000000250efaffffff0d7205213f0804bc14cdcd22b1305d1b0b05a701d0c2320000000cd721aa


    def __init__(self):
        pass
        self.info = {}

    def parse_ascii( self,msg):
        # "".strip
        try:
            msg = msg.lstrip("AT_ITINS=")
            # print(msg)
            # return None

            text_array = msg.split(',')
            if text_array.__len__() != 16 :
                print("error :text_array len = %s " % text_array.__len__())
                return {}

            info = {}
            info['lat'] = float(text_array[0])
            info['lon'] = float(text_array[1])
            info['v'] = float(text_array[7])
            info['azi_a'] = float(text_array[11])
            info['cnt'] = 0
            info["gps_state"] = 4
            return info.copy()

        except Exception as e:
            print(e)
            return {}

    def parse(self, msg):


        try:

            info = {}

            data = bytearray.fromhex(msg)[4:]

            info['lat_ins']  = int.from_bytes(data[30:34],byteorder='little',signed=True)/ 10000000.0
            info['lon_ins']  = int.from_bytes(data[34:38],byteorder='little',signed=True)/ 10000000.0
            # altitude
            info['h_ins']  = int.from_bytes(data[38:42],byteorder='little',signed=True)/ 100.0

            info['lat_gnss']  = int.from_bytes(data[54:58],byteorder='little',signed=True)/ 10000000.0
            info['lon_gnss']  = int.from_bytes(data[58:62],byteorder='little',signed=True)/ 10000000.0

            info['lat'] = info['lat_ins'] 
            info['lon'] = info['lon_ins'] 
            info['h'] = info['h_ins'] 

            # info['lat'] = info['lat_gnss'] 
            # info['lon'] = info['lon_gnss'] 


            info['East_speed']  = int.from_bytes(data[42:46],byteorder='little',signed=True) / 100.0# m/s
            info['North_speed']  = int.from_bytes(data[46:50],byteorder='little',signed=True)/100.0 # m/s
            info['Vertical_speed']  = int.from_bytes(data[50:54],byteorder='little',signed=True)/ 100.0 # m/s
            info['v'] = ( (info['East_speed']  ** 2 + info['North_speed'] ** 2) ** 0.5 ) * 3.6 # km/h

            gps_status = data[82] & 0x07
            solnSVs = data[84]
            # solnSVs


            info['Heading_GNSS'] = int.from_bytes(data[88:90],byteorder='little',signed=False) / 100.0
            # info['Pitch_GNSS'] = int.from_bytes(data[90:92],byteorder='little',signed=True) / 100.0 
            # info['Heading_STD_GNSS'] = int.from_bytes(data[92:94],byteorder='little',signed=False) 
            # info['Pitch_STD_GNSS'] = int.from_bytes(data[94:96],byteorder='little',signed=False) 

            # import math
            # RAD2DEG = 180/math.pi
            # DEG2RAD = math.pi/180

            info['heading'] = int.from_bytes(data[0:2],byteorder='little',signed=False) / 100.0
            info['pitch'] = int.from_bytes(data[2:4],byteorder='little',signed=True) / 100.0
            info['roll'] = int.from_bytes(data[4:6],byteorder='little',signed=True) / 100.0

            # // imu数值范围　转换为 [-180 , 180]
            _heading_deg = info['heading']
            '''
            def rotate_deg. heading2yaw(heading_deg):
                _gnss_yaw = 0
                if heading_deg > 180:
                    _gnss_yaw =  360 - heading_deg
                else:
                    _gnss_yaw = 0 - heading_deg
                if _gnss_yaw == -180:
                    _gnss_yaw = 180
                return _gnss_yaw
            # info['yaw'] = heading2yaw(_heading_deg) 
            '''
            # info['yaw'] = rotate_deg.heading2yaw(_heading_deg) 

            # info['yaw'] = info['yaw'] * DEG2RAD
            # info['pitch'] = info['pitch'] * DEG2RAD
            # info['roll'] = info['roll'] * DEG2RAD

            info['acc_x'] = int.from_bytes(data[12:14],byteorder='little',signed=True)/4000*9.8 
            info['acc_y'] = int.from_bytes(data[14:16],byteorder='little',signed=True)/4000*9.8
            info['acc_z'] = int.from_bytes(data[16:18],byteorder='little',signed=True) /4000*9.8 -9.8


            info['usw'] = int.from_bytes(data[24:26],byteorder='little',signed=False) 

            info['azi_a'] =info['heading']  
            # info['azi_a'] =info['Heading_GNSS']  

            # print("heading: %.2f  : Heading_GNSS %.2f " % (info['heading'],info['Heading_GNSS']))

            info['cnt'] = 0
            info['cnt'] = solnSVs
            if info['usw'] :
                print("error usw =  %s" %info['usw'] )

            # info["gps_state"] =gps_status
            GPS_STAUTS = [0,1,2,3,5,4,7,8,9,10] 
            info["gps_state"] =GPS_STAUTS[gps_status] 

            # print(info)
            return info.copy()
        except Exception as e:
            print(e)
            return {}



def log_init():
    from utils.loginit import init_logger
    import platform
    if platform.system() == "Windows":
        init_logger("C:\\Users\\Administrator\Desktop\\gps.log")
    elif platform.system() == "Linux":
        init_logger("/tmp/gps.log")
    elif platform.system() == "Darwin":
        init_logger("/tmp/gps.log")
    else:
        init_logger()
def test_main():
    gps_pares = GpsPares_NewtonM2()
    msg = "$GPFPD,2117,546559.380,81.348,1.461,0.295,36.70388968,117.14939980,38.02,0.005,0.001,-0.002,1.366,22,24,4B*2B"
    ret = gps_pares.parse(msg)
    print(ret)

    pass
if __name__ == "__main__":
    test_main()

    pass


