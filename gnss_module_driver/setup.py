## ! DO NOT MANUALLY INVOKE THIS setup.py, USE CATKIN INSTEAD
from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

# fetch values from package.xml
setup_args = generate_distutils_setup (
    packages = ['gnss_carla_driver',
                'gnss_lgsvl_driver',
                'gnss_nmea_driver',
                'gnss_ins_driver',
                'gnss_ncom_driver',
                'gnss_P2_driver',
                'gnss_NewtonM2_driver'] ,
    package_dir = {'': 'src'} ,
)
setup ( **setup_args )