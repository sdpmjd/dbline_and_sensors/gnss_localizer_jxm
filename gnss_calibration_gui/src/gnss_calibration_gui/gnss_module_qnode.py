#!/usr/bin/env python3

from PyQt5.QtCore import QThread,pyqtSignal,QTimer

import rospy
from std_msgs.msg import String
from sensor_msgs.msg import Imu
from gps_common.msg import GPSFix
from geometry_msgs.msg import PoseStamped

import math

class QNode(QThread):
    trigger = pyqtSignal ( dict , name="qnode_info" )
    def __init__(self,argv):
        super(QNode, self).__init__()
        self.init_argv = argv
        self.__info = {}
        rospy.init_node ( 'gnss_calibration_gui' , anonymous=True)
    
        self.ndt_pose = PoseStamped()
        self.fix = GPSFix()

        self._input_topic_imu_raw = rospy.get_param('~input_topic_imu_raw', "/gnss/imu_raw") 
        self._input_topic_fix = rospy.get_param('~input_topic_fix', '/gnss/fix')

        rospy.Subscriber ( self._input_topic_imu_raw, Imu , self.imu_callback,queue_size=1)
        rospy.Subscriber ( self._input_topic_fix, GPSFix , self.gpsfix_callback,queue_size=1)
        rospy.Subscriber ( 'ndt_pose', PoseStamped , self.ndt_pose_callback,queue_size=1)

        rospy.loginfo("init")

        self.timer = QTimer()
        self.timer.timeout.connect(self.qtimer_callback)
        self.timer.setInterval(100)
        self.timer.start()

    def run(self) -> None:
        rospy.spin()

    def qtimer_callback(self):
        self.trigger.emit(self.__info.copy())

    def imu_callback(self,imu):
        self.__info['acc_x']  =  imu.linear_acceleration.x
        self.__info['acc_y']  =  imu.linear_acceleration.y
        self.__info['acc_z']  =  imu.linear_acceleration.z

    def ndt_pose_callback(self,pose):
        self.ndt_pose = pose

    def gpsfix_callback(self,fix):
        self.fix = fix

        self.__info['lat']       =  fix.latitude
        self.__info['lon']       =  fix.longitude
        self.__info['altitude']  =  fix.altitude
        self.__info['speed']     =  fix.speed * 3.6
        self.__info['gps_state'] =  fix.status.status
        self.__info['azimutha']  =  fix.track
        self.__info['yaw']       =  fix.dip
        self.__info['pitch']     =  fix.pitch
        self.__info['roll']      =  fix.roll

    def append_gnss_ndt_pose(self):
        import os,json
        json_file = "/home/autoware/shared_dir/gnss_ndt_pose.json" ;
        fd = None

        json_dict = {'poses' : []}
        if not os.path.exists( json_file) :
            fd = open(json_file, "w")
        else:
            fd = open(json_file, "r")
            text = fd.read()
            if text:
                json_dict = json.loads(text)
        fd.close()

        gnss = {'altitude': self.fix.altitude, 
                    'lat':  self.fix.latitude, 
                    'lon':  self.fix.longitude, 
                    'pitch': 0.0, 
                    'roll': 0.0,
                    'yaw': math.radians(self.fix.dip)
                    } 

        slam =  {'orientation': {
                            'w': self.ndt_pose.pose.orientation.w, 
                            'x': self.ndt_pose.pose.orientation.x, 
                            'y': self.ndt_pose.pose.orientation.y, 
                            'z': self.ndt_pose.pose.orientation.z, 
                            }, 
                    'position': {'x': self.ndt_pose.pose.position.x, 
                                 'y': self.ndt_pose.pose.position.y, 
                                 'z': self.ndt_pose.pose.position.z
                                 }
                }
             
        json_dict['poses'].append( {"gnss":gnss, "slam": slam} ) 

        json_str = json.dumps(json_dict)
        # print(json_str)
        fd = open(json_file, "w+")

        fd.write(json_str)
        fd.flush()
        fd.close()


if __name__ == "__main__":
    from PyQt5.QtCore import QCoreApplication
    import sys
    core_app = QCoreApplication(sys.argv)
    thread = QNode(sys.argv)
    thread.start()
    core_app.exec()
