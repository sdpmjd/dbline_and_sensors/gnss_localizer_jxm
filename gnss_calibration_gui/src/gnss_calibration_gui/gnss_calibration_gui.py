#!/usr/bin/env python3
#coding=utf-8

import os, sys

from python_qt_binding import loadUi
from PyQt5.QtWidgets import QApplication,QWidget,QDialog,QCheckBox,QFileDialog
from PyQt5.QtCore import pyqtSignal,pyqtSlot,QThread,QTimer,Qt,QFile,QIODevice
from gnss_calibration_gui.gnss_module_qnode import QNode
from geographiclib.geodesic import Geodesic

import subprocess

import math
RAD2DEG = 180/math.pi


class GnssRecord():
    def __init__(self):
        self.geod = Geodesic.WGS84
        self.file = None
        self.file2 = None
        self.old_lon = None
        self.old_lat = None

        self.__record_enable = False
        self.sub_node_lgsvl = None
        self.sub_gnss2tfpose = None

    def set_enable( self ):

        self.__record_enable = True
        if self.file == None:
            return
        self.file.open ( QIODevice.WriteOnly | QIODevice.Text )
        self.file2.open ( QIODevice.WriteOnly | QIODevice.Text )


    def set_disable( self ):
        self.__record_enable = False

        if self.file != None:
            self.file.close ()

        if self.file2 != None:
            self.file2.close ()
        self.file = None
        self.file2 = None

        self.old_lat = None
        self.old_lon = None
    def next_gps_point(self,cur_lat,cur_lon,azi=0,len=2.2):
        '''
        :param cur_gps_point:  当前坐标 dirc
        :param azi:            航向角
        :param len:            距离
        :return:
        '''
        # g = Geodesic.Direct(lat1=cur_lat, lon1=cur_lon, azi1=azi, s12=len)
        g = self.geod.Direct(lat1=cur_lat, lon1=cur_lon, azi1=azi, s12=len)
        info ={}
        info['lat'] =  g['lat2']
        info['lon'] =  g['lon2']
        return info

    def save_to_file( self ,gps_info):
        if self.__record_enable == False:
            return

        if gps_info.get('lat') == None:
            return

        if gps_info.get('lon') == None:
            return

        if gps_info.get('azimutha') == None:
            return

        front_point = self.next_gps_point ( gps_info['lat'] , gps_info['lon'] , gps_info['azimutha'] , 2 )
        back_lat = gps_info['lat']
        back_lon = gps_info['lon']

        if gps_info['gps_state'] != 0  :
            print ( "gps state NOT FIX == %s" % gps_info['gps_state'] )
            return

        if self.file == None:
            return
        if self.old_lat == None:
            self.old_lat = back_lat
            self.old_lon = back_lon
            return
        ######################
        ret = self.geod.Inverse ( self.old_lat , self.old_lon , back_lat , back_lon )
        len = ret['s12']
        if len < 0.02:
            return
        self.old_lat = back_lat
        self.old_lon = back_lon

        self.save_gpsdata ( self.file , back_lat , back_lon , azi=gps_info['azimutha'] )

    def save_gpsdata( self,file,lat,lon,azi=0 ):
        try:
            info = "%.8f %.8f\n" %(float(lon),float(lat))
            file.write(info.encode("utf-8"))
            file.flush()
        except Exception as e:
            print(e)

import yaml
class YamlCfg:
    @classmethod
    def save(cls,lat,lon,h,roll,pitch,yaw,ndt_pose,yaml_path):
        f = open ( yaml_path ,"w")
        info = { 
            "localcartesian_lat0": float(lat), 
            "localcartesian_lon0": float(lon), 
            "localcartesian_h0":  float(h),
            "localcartesian_roll": float(roll), 
            "localcartesian_pitch": float(pitch), 
            "localcartesian_yaw": float(yaw), 


            "tf_ndt_gnss":{
                "position":{
                    "x":ndt_pose.pose.position.x,
                    "y":ndt_pose.pose.position.y,
                    "z":ndt_pose.pose.position.z
                },
                "orientation":{
                        "x":ndt_pose.pose.orientation.x,
                        "y":ndt_pose.pose.orientation.y,
                        "z":ndt_pose.pose.orientation.z,
                        "w":ndt_pose.pose.orientation.w
                }
            }
        }

        yaml.dump(info, f)
        f.flush()
        f.close()


class GNSS(QDialog):
    def __init__(self):
        super(GNSS, self).__init__()

        ''' Load Gui'''

        self._node = QNode(sys.argv)
        import rospy
        self.ui_dir = rospy.get_param('~ui_path', 'ui')

        # self_dir = os.path.dirname(os.path.realpath(__file__)) // 获取绝对路径
        # self.ui_dir = os.path.join(self_dir, '../../ui')
        ui_file = os.path.join(self.ui_dir, 'main.ui')
        rospy.loginfo("ui_file: %s" % self.ui_dir)
        loadUi(ui_file, self)
        # self.__yaml_dir = os.path.join(self_dir, '../../yaml')

        # print os.environ['HOME']
        self.__yaml_dir = os.path.expandvars('$HOME')+"/.config/gnss"
        if not os.path.exists(self.__yaml_dir):
            # os.makedirs(YAML_DIR, mode=0o775)
            os.makedirs(self.__yaml_dir)
            _h0 = 40.64
            _lat0 = 36.70261262
            _lon0 = 117.14650666

            rospy.loginfo("create yaml file : %s" % self.__yaml_dir + "/config.yaml")
            YamlCfg.save(_lat0,_lon0, _h0,0,0,0,self._node.ndt_pose,self.__yaml_dir + "/config.yaml")

        # self.setWindowFlags ( Qt.FramelessWindowHint | Qt.WindowSystemMenuHint | Qt.WindowMinimizeButtonHint )

        self._node.trigger.connect(self.update_ui)
        self._node.start()

        self.gnss_record = GnssRecord()

        # self.checkBox_sim.setHidden( rospy.get_param('~checkBox_sim_hidden', True))
        self.checkBox_sim.setHidden( True)
        self.lineEdit_dev_h.setHidden( True)
        self.label_gps_device_h.setHidden( True)
        # self.button_browser.setHidden( True)
        self.widget_3.setHidden( True)
        self.widget_2.setHidden( True)

        self.label_17.setHidden( True)
        self.label_cnt.setHidden( True)

    def __del__(self):
        print("============== kill rosnodes ================= ")
        subprocess.Popen(["rosnode", "kill", "websocket_bridge"])
        subprocess.Popen(["rosnode", "kill", "republish"])
        subprocess.Popen(["rosnode", "kill", "gnss2tfpose_node"])
 

    def update_ui ( self , gps_info ):
        try:
            '''
            info = {'lat': 36.4223085003, 'lng': 117.08943584500001, 
                    'speed': 0.02005, 
                    'azimutha': 89.6393, 
                    'gps_state': 5}
            '''
            if (gps_info.get('gps_state') == 0):
                self.label_gps_status.setText ( "FIX" )
                self.label_gps_status.setStyleSheet (
                    "QLabel {background-color: rgb(255, 255, 255); color: black;font:bold }" )
            else:
                self.label_gps_status.setText ( "NO FIX : %s" % gps_info.get('gps_state',"None") )
                self.label_gps_status.setStyleSheet(
                    "QLabel {background-color: rgb(255, 123, 2); color: white;font:bold }")

            self.label_lng.setText ( "%.8f" % gps_info.get('lon',0) )
            self.label_lat.setText ( "  %.8f" % gps_info.get('lat',0) )
            self.label_h.setText ( "  %.8f" % gps_info.get('altitude',0) )
            self.label_v.setText ( "%.2f" % gps_info.get('speed',0) )
            self.label_azi.setText ( "%.2f" % gps_info.get('azimutha',0) )
            self.label_cnt.setText ( "%d" % gps_info.get('cnt',10) )
            if  "yaw" in gps_info.keys():
                self.label_Yaw.setText ( "%.2f" % gps_info.get('yaw',0) )
            if  "pitch" in gps_info.keys():
                self.label_Pitch.setText ( "%.2f" % gps_info.get('pitch',0) )
            if  "roll" in gps_info.keys():
                self.label_Roll.setText ( "%.2f" % gps_info.get('roll',0) )

            if  "acc_x" in gps_info.keys():
                self.label_acc_x.setText ( "%.2f" % gps_info.get('acc_x',0) )
            if  "acc_y" in gps_info.keys():
                self.label_acc_y.setText ( "%.2f" % gps_info.get('acc_y',0) )
            if  "acc_z" in gps_info.keys():
                self.label_acc_z.setText ( "%.2f" % gps_info.get('acc_z',0) )

            # if gps_info[]
            self.gnss_record.save_to_file(gps_info)

        except Exception as e:
            print(e)

    @pyqtSlot()
    def on_button_browser_clicked(self):

        from PyQt5.QtCore import  QStandardPaths,QDir
        desktop_path = QStandardPaths.writableLocation(QStandardPaths.DesktopLocation) # 获取桌面
        print(desktop_path)

        ret = QFileDialog.getSaveFileName(self,"选择保存文件路径",desktop_path + QDir("/test.gpsdata").path() )
        file_path = ret[0]
        if file_path != '':
            # if (QFile.exists(file_path)){
            # }
            # self.file = QFile(file_path)
            # self.file2 = QFile("f_"+file_path)
            self.gnss_record.file = QFile(file_path)
            self.gnss_record.file2 = QFile(file_path)

            self.label_file_path.setText(file_path)

    @pyqtSlot()
    def on_button_start_clicked ( self ):

        self.gnss_record.set_enable()

        self.button_start.setDisabled ( True )
        self.button_browser.setDisabled ( True )

    @pyqtSlot()
    def on_button_stop_clicked ( self ):

        self.gnss_record.set_disable()

        self.button_start.setDisabled ( False )
        self.button_browser.setDisabled ( False )

    @pyqtSlot()
    def on_pushButton_set_coordinate_origin_clicked ( self ):
        lon = float(self.label_lng.text ())
        lat = float(self.label_lat.text ())
        h = float(self.label_h.text ()) #- float(self.lineEdit_dev_h.text())
        yaw = float(self.label_Yaw.text ())/RAD2DEG

        # YamlCfg.save(lat,lon,h,0,0,yaw,self._node.ndt_pose,self.__yaml_dir + "/config.yaml")
        self._node.append_gnss_ndt_pose()


    @pyqtSlot(int)
    def on_checkBox_sim_stateChanged ( self,status ):
        try:
            if (status == 0):
                print(self.sub_node_lgsvl)
                if 'sub_node_lgsvl' in self.__dict__.keys() and self.sub_node_lgsvl != None :
                    subprocess.Popen(["rosnode", "kill", "websocket_bridge"])
                    subprocess.Popen(["rosnode", "kill", "republish"])
            else:
                if 'sub_node_lgsvl' in self.__dict__.keys() and self.sub_node_lgsvl != None :
                    self.sub_node_lgsvl.kill()
                self.sub_node_lgsvl = subprocess.Popen(["roslaunch","gnss_module_gui" ,"lgsvl_websocket_bridge.launch"])
        except Exception as e:
            print("==================",e)


    @pyqtSlot(int)
    def on_checkBox_2tfpose_stateChanged ( self,status ):
        try:
            if (status == 0):
                if 'sub_gnss2tfpose' in self.__dict__.keys() and self.sub_gnss2tfpose != None :
                    print('\n============= kill gnss2tfpose_node============\n')
                    subprocess.Popen(["rosnode", "kill", "gnss2tfpose_node"])
            else:
                if 'sub_gnss2tfpose' in self.__dict__.keys() and self.sub_gnss2tfpose != None :
                    self.sub_gnss2tfpose.kill()
                self.sub_gnss2tfpose = subprocess.Popen(["roslaunch","gnss2tfpose" ,"start.launch"])
        except Exception as e:
            print(e)


def main():
    app=QApplication(sys.argv)
    win = GNSS()
    win.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    print("start =====================\n")
    main()