#!/usr/bin/env python3

from PyQt5.QtCore import QThread,pyqtSignal,QTimer

import rospy
from std_msgs.msg import String
from pm_gnss_msgs.msg import GnssInfo
from gnss_module_lgsvl.gnss_pares import GpsPares,GpsPares_INS,GpsPares_NewtonM2
# from gps_msg_sender import GpsMsgSender
# from geometry_msgs.msg import PoseStamped
# from tf.transformations import euler_from_quaternion, quaternion_from_euler


class QNode(QThread):
    trigger = pyqtSignal ( dict , name="qnode_info" )
    def __init__(self,argv):
        super(QNode, self).__init__()
        self.init_argv = argv
        self.__info = {}
        rospy.init_node ( 'gps_module_qnode' , anonymous=True)
        rospy.Subscriber ( "/gnss_info", GnssInfo , self.Callback,queue_size=1)
        rospy.loginfo("init")

        self.timer = QTimer()
        self.timer.timeout.connect(self.qtimer_callback)
        self.timer.setInterval(100)
        self.timer.start()


    def run(self) -> None:
        #rospy.loginfo("run : thread pid %s ...." % os.getpid())
        rospy.spin()

    def qtimer_callback(self):
        if self.__info.__len__() >= 6: 
            self.trigger.emit(self.__info.copy())

    def Callback( self,gnss_info):
        self.__info['lat']       =  gnss_info.lat
        self.__info['lon']       =  gnss_info.lon
        self.__info['altitude']  =  gnss_info.h
        self.__info['speed']     =  gnss_info.velocity
        self.__info['gps_state'] =  gnss_info.status
        self.__info['azimutha']  =  gnss_info.azi
        self.__info['yaw']       =  gnss_info.yaw
        self.__info['pitch']     =  gnss_info.pitch
        self.__info['roll']      =  gnss_info.roll


if __name__ == "__main__":
    from PyQt5.QtCore import QCoreApplication
    import sys
    core_app = QCoreApplication(sys.argv)
    thread = QNode(sys.argv)
    thread.start()
    core_app.exec()
