#!/usr/bin/env python2
#coding=utf-8

import sys
import rospy
from std_msgs.msg import String
from nmea_msgs.msg import Sentence
from pm_gnss_msgs.msg import GnssInfo
from gnss_module_lgsvl.gnss_pares import GpsPares,GpsPares_INS,GpsPares_NewtonM2
# from scripts.gps_msg_sender import GpsMsgSender
from tf.transformations import euler_from_quaternion, quaternion_from_euler

from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry

import math
RAD2DEG = 180/math.pi

class Node(object):
    def __init__(self):
        self.__info = {}
        rospy.init_node ( 'gps_module_core' ,log_level=rospy.INFO ,anonymous=True)

        rospy.loginfo("init")
        gnss_type = rospy.get_param('~gnss_type', 'NMEA')
        rospy.loginfo("gnss_type : %s" %gnss_type)
        self.d_yaw = rospy.get_param('~d_yaw', 0)  # default lgsvl 2020.06

        if gnss_type == "NMEA": 
            self.gps_pares =  GpsPares() # NMEA
        elif  gnss_type == "NewtonM2" :
            self.gps_pares =  GpsPares_NewtonM2() # NewtonM2
        else:
            self.gps_pares =  GpsPares_INS() # 北斗
        # self.gnss_udp_send =  GpsMsgSender()

        self._cnt = 0

        rospy.Subscriber ( "/imu_raw", Imu , self.imu_callback,queue_size=1)
        # rospy.Subscriber ( "/odom", Odometry , self.odom_callback,queue_size=1)
        rospy.Subscriber ( "/vehicle/odom", Odometry , self.odom_callback,queue_size=1)
        rospy.Subscriber ( "/nmea_sentence", Sentence , self.Callback,queue_size=1)
        self.gnss_info_pub = rospy.Publisher ( '/gnss_info' , GnssInfo , queue_size=1 )
        rospy.Timer ( rospy.Duration ( secs = 0, nsecs=50*1000000 ) , self.timer_callback )  # send to socket

    def run(self):
        #rospy.loginfo("run : thread pid %s ...." % os.getpid())
        rospy.spin()

    def parse( self,msg_string ):
        try:
            info = self.gps_pares.parse(msg_string)
            if info.__len__() == 0:
                return
            # print(info)
            self.__info['lat']       = float(info.get('lat', self.__info.get('lat',0) ) ) 
            self.__info['lon']       = float(info.get('lon',self.__info.get('lon',0))) 
            self.__info['h']       = float(info.get('h',self.__info.get('h',0))) 
            # self.__info['speed']     = float(info.get('v',  self.__info.get('speed',0)))
            self.__info['gps_state'] = int(info.get('gps_state',self.__info.get('gps_state',0)))
            # self.__info['azimutha']  = float(info.get('azi_a', self.__info.get('azimutha',0)))
            self.__info['cnt']       = float(info.get('cnt',self.__info.get('cnt',0)))

            # if  "yaw" in info.keys():
            #     self.__info['yaw'] = info["yaw"]
            # if  "pitch" in info.keys():
            #     self.__info['pitch'] = info["pitch"]
            # if  "roll" in info.keys():
            #     self.__info['roll'] = info["roll"]

            self.__info['gps_state'] = 4
            # if self.__info['gps_state'] != 4:
            #     msg = "error gps_state %s " % self.__info.get('gps_state')
            #     rospy.logerr(msg)
            #     # logging.error("GPS state = %s " % self.gps_info['gps_state'] )
        except Exception as e:
            print(e)

    def timer_callback( self ,event):
        try:
            gnss_info = GnssInfo()
            gnss_info.lat = self.__info.get('lat',0)
            gnss_info.lon = self.__info.get('lon',0)
            gnss_info.h = self.__info.get('h',0)
            gnss_info.velocity = self.__info.get('speed',0)
            gnss_info.status   = self.__info.get('gps_state',GnssInfo.INIT)
            gnss_info.azi      = self.__info.get('azimutha',0)
            gnss_info.yaw      = self.__info.get('yaw',0)
            gnss_info.pitch    = self.__info.get('pitch',0)
            gnss_info.roll     = self.__info.get('roll',0)

            self.gnss_info_pub.publish(gnss_info)

            # print(self.__info)
            # rospy.loginfo(self.__info)
            # if self.__info.__len__() >= 5:
            #     self.gnss_udp_send.send_info(self.__info.copy())

        except  Exception as e:
            print(e)
        finally:
            pass

    def Callback( self,data):
        self.parse(data.sentence)

    def imu_callback(self,imu): 
        roll,pitch,yaw  = self.get_euler(imu.orientation)
        yaw_deg = yaw * RAD2DEG
        pitch_deg = pitch * RAD2DEG
        roll_deg = roll * RAD2DEG

        # def reverse(deg):
        #     if deg >= 0:
        #         return 180 - deg
        #     if deg < 0:
        #         return -(180 + deg)

        # 旋转
        def rotate(base_deg, _deg):
            # if _deg  = 180 or deg = -180 :
                # pass
            if _deg >= 0 and _deg < 180:
                _deg = _deg % 180
            if _deg  < 0 and _deg > -180:
                _deg = _deg % -180

            base_deg += _deg
            if base_deg >= 180:
                return -180 + (base_deg%180)
            elif base_deg < -180:
                return 180 + (base_deg%-180)
            else:
                return base_deg

        # d_yaw = -90 #2020.05
        # d_yaw =  0   #2020.06
        # self.__info['yaw']   = rotate(reverse (yaw_deg),self.d_yaw) 
        self.__info['yaw']   = rotate(yaw_deg,self.d_yaw) 
        self.__info['pitch'] = pitch_deg 
        self.__info['roll']  = roll_deg 
    
        ###########航向角#####################
        azi = rotate(self.__info['yaw'],-90) 

        # [-180 - 180 ] => [0, 360]
        if azi < 0:
            azi = 360+azi 
        self.__info['azimutha'] = 360 - azi

    # def imu_callback2(self,imu): 
    #     roll,pitch,yaw  = self.get_euler(imu.orientation)
    #     yaw_deg = yaw * RAD2DEG
    #     pitch_deg = pitch * RAD2DEG
    #     roll_deg = roll * RAD2DEG

    #     def reverse(deg):
    #         # deg =-deg
    #         if deg >= 0:
    #             return 180 - deg
    #         if deg < 0:
    #             return -(180 + deg)

    #     def rotate(base_deg, _deg):
    #         # if _deg  = 180 or deg = -180 :
    #             # pass
    #         if _deg >= 0 and _deg < 180:
    #             _deg = _deg % 180
    #         if _deg  < 0 and _deg > -180:
    #             _deg = _deg % -180

    #         base_deg += _deg
    #         if base_deg >= 180:
    #             return -180 + (base_deg%180)
    #         elif base_deg < -180:
    #             return 180 + (base_deg%-180)
    #         else:
    #             return base_deg

    #     # d_yaw = -90 #2020.05
    #     # d_yaw =  0   #2020.06
    #     self.__info['yaw']   = rotate(reverse (yaw_deg),self.d_yaw) 
    #     self.__info['pitch'] =-pitch_deg # reverse(pitch_deg)
    #     self.__info['roll']  = roll_deg 
    
    #     ###########航向角#####################
    #     azi = rotate(self.__info['yaw'],-90) 
    #     # print(azi)
    #     if azi < 0:
    #         azi = 360+azi 
    #     self.__info['azimutha'] = azi

    # 四元数转RPY欧拉角
    def get_euler (self,orientation):
        orientation_q =orientation
        orientation_list = [orientation_q.x, orientation_q.y, orientation_q.z, orientation_q.w]
        (roll, pitch,yaw ) = euler_from_quaternion (orientation_list)

        return (roll, pitch,  yaw )

    def odom_callback(self,odom):
       self.__info['speed'] = odom.twist.twist.linear.x * 3.6

def main():
    node = Node()
    node.run()

if __name__ == "__main__":
    main()