# Readme

## Install Protobuf

* [How to install protobuf](https://my-space.readthedocs.io/zh/latest/cpp/index.html#protobuf)

##  Git submodule of protobuf file

```sh

	git submodule add https://gitee.com/sdpromote/proto.git proto
	git submodule add -b v1.0  https://gitee.com/sdpromote/proto.git proto
	git submodule add -b v1.0 --force https://gitee.com/sdpromote/proto.git proto

	##########################
	#    跟新，子模块
	##########################
	git submodule foreach git pull origin master # 拉取所有子模块
	git submodule sync                      #
	git add .                               #
	git push origin master                  #


	git submodule update --recursive --remote  # 更新子模块

```





