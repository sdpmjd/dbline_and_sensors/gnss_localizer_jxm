#include <iostream> 
#include <fstream>
#include <string>
#include "gps_path_tracking.pb.h"
using namespace std;

int main(int argc, char* argv[]) {
  // Verify that the version of the library that we linked against is
  // compatible with the version of the headers we compiled against.
   GOOGLE_PROTOBUF_VERIFY_VERSION;

  // ＠＠＠ AddressBook
  //tutorial::AddressBook address_book;
   //GpsPoint gps_point;
#if 1
  GpsPathTrackingInfo gps_path_tracking_info;
  GpsPathTrackingInfo gps_path_tracking_info2;

  GpsPoint * cur_gps_point = gps_path_tracking_info.add_cur_gps_point(); cur_gps_point->set_lat(36.12344);
  cur_gps_point->set_lon(120.12344);

  GpsPoint * cur_near_gps_point = gps_path_tracking_info.add_cur_near_gps_point();
  cur_near_gps_point->set_lat(37.12344);
  cur_near_gps_point->set_lon(121.12344);


  GpsPoint * per_gps_point = gps_path_tracking_info.add_pre_gps_point();
  per_gps_point->set_lat(38.12344);
  per_gps_point->set_lon(122.12344);

  gps_path_tracking_info.set_min_distance(1.23);
  gps_path_tracking_info.set_finished(false);
  cout << sizeof(gps_path_tracking_info) << endl;

  //gps_path_tracking_info
  cout<< gps_path_tracking_info.SpaceUsed() << endl;

  //************* Convert *****************//
  string str;
  gps_path_tracking_info.SerializeToString(&str); //
  gps_path_tracking_info2.ParseFromString(str);

  cout << gps_path_tracking_info2.min_distance() << " " << endl;
  cout << gps_path_tracking_info2.finished() << " " << endl;

  const GpsPoint& _cur_gps_point = gps_path_tracking_info.cur_gps_point(0);
  const GpsPoint& _cur_near_gps_point = gps_path_tracking_info2.cur_near_gps_point(0);
  const GpsPoint& _pre_gps_point = gps_path_tracking_info2.pre_gps_point(0);
  cout << _cur_gps_point.lat() << " " << _cur_gps_point.lon() <<  endl;
  cout << _cur_near_gps_point.lat() << " " << _cur_near_gps_point.lon() <<  endl;
  cout << _pre_gps_point.lat() << " " << _pre_gps_point.lon() <<  endl;
  //test_point.set_lat(123.11);
  
  GpsInfo * point = gps_path_tracking_info.mutable_gps_info();
  point->set_lat(123.11);
  point->set_lon(123.456);

  const GpsInfo &test_point = gps_path_tracking_info.gps_info();
  cout << test_point.lat()<<endl;
  cout << gps_path_tracking_info.gps_info().lat()<<endl;

  //gps_path_tracking_info.test_point.set_lon(111.2);
  /***********************************************************************/
  // std::vector<uint8_t> buff;
  // buff.resize(gps_path_tracking_info.ByteSize());
  // gps_path_tracking_info.SerializeToArray(buff.data(), buff.size());  
  //cout << "buff size : " << buff.size()<<endl;
  //gps_path_tracking_info2.ParseFromArray(&buff,buff.size());
  
#endif
  // Optional:  Delete all global objects allocated by libprotobuf.
  // ＠＠＠ ShutdownProtobufLibrary
  google::protobuf::ShutdownProtobufLibrary();

  return 0;
}
