#include <iostream>
#include <fstream>
#include <string>
#include "gps_path_tracking.pb.h"

using namespace std;

void ListPeople(const GpsPathTrackingInfo& gps_path_tracking_info) {
    for (int i = 0; i < gps_path_tracking_info.cur_gps_point_size(); i++) {
        const GpsPoint& cur_gps_point = gps_path_tracking_info.cur_gps_point(i);

        cout << cur_gps_point.lat() << " " << cur_gps_point.lon() << endl;
    }
}

int main(int argc, char **argv) {
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    if (argc != 2) {
        cerr << "Usage: " << argv[0] << " ADDRESS_BOOL_FILE" << endl;
        return -1;
    }

    GpsPathTrackingInfo gps_path_tracking_info;

    {
        fstream input(argv[1], ios::in | ios::binary);
        if (!gps_path_tracking_info.ParseFromIstream(&input)) {
            cerr << "Filed to parse address book." << endl;
            return -1;
        }
        input.close();
    }

    ListPeople(gps_path_tracking_info);

    // Optional: Delete all global objects allocated by libprotobuf.
    //google::protobuf::ShutdownProtobufLibrary();

    return 0;
}

