// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: gps.proto

#ifndef PROTOBUF_gps_2eproto__INCLUDED
#define PROTOBUF_gps_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/metadata.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/unknown_field_set.h>
// @@protoc_insertion_point(includes)

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_gps_2eproto();
void protobuf_AssignDesc_gps_2eproto();
void protobuf_ShutdownFile_gps_2eproto();

class GpsInfo;

// ===================================================================

class GpsInfo : public ::google::protobuf::Message /* @@protoc_insertion_point(class_definition:GpsInfo) */ {
 public:
  GpsInfo();
  virtual ~GpsInfo();

  GpsInfo(const GpsInfo& from);

  inline GpsInfo& operator=(const GpsInfo& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _internal_metadata_.unknown_fields();
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields();
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const GpsInfo& default_instance();

  void Swap(GpsInfo* other);

  // implements Message ----------------------------------------------

  inline GpsInfo* New() const { return New(NULL); }

  GpsInfo* New(::google::protobuf::Arena* arena) const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const GpsInfo& from);
  void MergeFrom(const GpsInfo& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* InternalSerializeWithCachedSizesToArray(
      bool deterministic, ::google::protobuf::uint8* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const {
    return InternalSerializeWithCachedSizesToArray(false, output);
  }
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(GpsInfo* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _internal_metadata_.arena();
  }
  inline void* MaybeArenaPtr() const {
    return _internal_metadata_.raw_arena_ptr();
  }
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // required double lat = 1;
  bool has_lat() const;
  void clear_lat();
  static const int kLatFieldNumber = 1;
  double lat() const;
  void set_lat(double value);

  // required double lon = 2;
  bool has_lon() const;
  void clear_lon();
  static const int kLonFieldNumber = 2;
  double lon() const;
  void set_lon(double value);

  // optional double azi = 3;
  bool has_azi() const;
  void clear_azi();
  static const int kAziFieldNumber = 3;
  double azi() const;
  void set_azi(double value);

  // optional double speed = 4;
  bool has_speed() const;
  void clear_speed();
  static const int kSpeedFieldNumber = 4;
  double speed() const;
  void set_speed(double value);

  // optional uint32 status = 5;
  bool has_status() const;
  void clear_status();
  static const int kStatusFieldNumber = 5;
  ::google::protobuf::uint32 status() const;
  void set_status(::google::protobuf::uint32 value);

  // @@protoc_insertion_point(class_scope:GpsInfo)
 private:
  inline void set_has_lat();
  inline void clear_has_lat();
  inline void set_has_lon();
  inline void clear_has_lon();
  inline void set_has_azi();
  inline void clear_has_azi();
  inline void set_has_speed();
  inline void clear_has_speed();
  inline void set_has_status();
  inline void clear_has_status();

  // helper for ByteSize()
  int RequiredFieldsByteSizeFallback() const;

  ::google::protobuf::internal::InternalMetadataWithArena _internal_metadata_;
  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  double lat_;
  double lon_;
  double azi_;
  double speed_;
  ::google::protobuf::uint32 status_;
  friend void  protobuf_AddDesc_gps_2eproto();
  friend void protobuf_AssignDesc_gps_2eproto();
  friend void protobuf_ShutdownFile_gps_2eproto();

  void InitAsDefaultInstance();
  static GpsInfo* default_instance_;
};
// ===================================================================


// ===================================================================

#if !PROTOBUF_INLINE_NOT_IN_HEADERS
// GpsInfo

// required double lat = 1;
inline bool GpsInfo::has_lat() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void GpsInfo::set_has_lat() {
  _has_bits_[0] |= 0x00000001u;
}
inline void GpsInfo::clear_has_lat() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void GpsInfo::clear_lat() {
  lat_ = 0;
  clear_has_lat();
}
inline double GpsInfo::lat() const {
  // @@protoc_insertion_point(field_get:GpsInfo.lat)
  return lat_;
}
inline void GpsInfo::set_lat(double value) {
  set_has_lat();
  lat_ = value;
  // @@protoc_insertion_point(field_set:GpsInfo.lat)
}

// required double lon = 2;
inline bool GpsInfo::has_lon() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void GpsInfo::set_has_lon() {
  _has_bits_[0] |= 0x00000002u;
}
inline void GpsInfo::clear_has_lon() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void GpsInfo::clear_lon() {
  lon_ = 0;
  clear_has_lon();
}
inline double GpsInfo::lon() const {
  // @@protoc_insertion_point(field_get:GpsInfo.lon)
  return lon_;
}
inline void GpsInfo::set_lon(double value) {
  set_has_lon();
  lon_ = value;
  // @@protoc_insertion_point(field_set:GpsInfo.lon)
}

// optional double azi = 3;
inline bool GpsInfo::has_azi() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void GpsInfo::set_has_azi() {
  _has_bits_[0] |= 0x00000004u;
}
inline void GpsInfo::clear_has_azi() {
  _has_bits_[0] &= ~0x00000004u;
}
inline void GpsInfo::clear_azi() {
  azi_ = 0;
  clear_has_azi();
}
inline double GpsInfo::azi() const {
  // @@protoc_insertion_point(field_get:GpsInfo.azi)
  return azi_;
}
inline void GpsInfo::set_azi(double value) {
  set_has_azi();
  azi_ = value;
  // @@protoc_insertion_point(field_set:GpsInfo.azi)
}

// optional double speed = 4;
inline bool GpsInfo::has_speed() const {
  return (_has_bits_[0] & 0x00000008u) != 0;
}
inline void GpsInfo::set_has_speed() {
  _has_bits_[0] |= 0x00000008u;
}
inline void GpsInfo::clear_has_speed() {
  _has_bits_[0] &= ~0x00000008u;
}
inline void GpsInfo::clear_speed() {
  speed_ = 0;
  clear_has_speed();
}
inline double GpsInfo::speed() const {
  // @@protoc_insertion_point(field_get:GpsInfo.speed)
  return speed_;
}
inline void GpsInfo::set_speed(double value) {
  set_has_speed();
  speed_ = value;
  // @@protoc_insertion_point(field_set:GpsInfo.speed)
}

// optional uint32 status = 5;
inline bool GpsInfo::has_status() const {
  return (_has_bits_[0] & 0x00000010u) != 0;
}
inline void GpsInfo::set_has_status() {
  _has_bits_[0] |= 0x00000010u;
}
inline void GpsInfo::clear_has_status() {
  _has_bits_[0] &= ~0x00000010u;
}
inline void GpsInfo::clear_status() {
  status_ = 0u;
  clear_has_status();
}
inline ::google::protobuf::uint32 GpsInfo::status() const {
  // @@protoc_insertion_point(field_get:GpsInfo.status)
  return status_;
}
inline void GpsInfo::set_status(::google::protobuf::uint32 value) {
  set_has_status();
  status_ = value;
  // @@protoc_insertion_point(field_set:GpsInfo.status)
}

#endif  // !PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_gps_2eproto__INCLUDED
