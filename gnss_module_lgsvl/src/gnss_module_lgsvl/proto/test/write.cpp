#include <iostream>
#include <fstream>
#include <string>
//#include "addressbook.pb.h"
#include "gps_path_tracking.pb.h"
using namespace std;

///*
// This function fills in a Person message based on user input.
void PromptForAddress(GpsPoint *gps_point) 
{
  cout << "Enter lat: ";
  double lat;
  cin >> lat;
  gps_point->set_lat(lat);
  cin.ignore(256, '\n');

  cout << "Enter lon: ";
  double lon;
  cin >> lon;
  gps_point->set_lon(lon);
  cin.ignore(256, '\n');

  // cout << "Enter name: ";
  // getline(cin, *person->mutable_name());

  // cout << "Enter email address (blank for none): ";
  // string email;
  // getline(cin, email);
  // if (!email.empty()) {
  //   person->set_email(email);
  // }

  // while (true) {
  //   cout << "Enter a phone number (or leave blank to finish): ";
  //   string number;
  //   getline(cin, number);
  //   if (number.empty()) {
  //     break;
  //   }
  //   // ＠＠＠ Person::PhoneNumber
  //   tutorial::Person::PhoneNumber* phone_number = person->add_phones();
  //   phone_number->set_number(number);

  //   cout << "Is this a mobile, home, or work phone? ";
  //   string type;
  //   getline(cin, type);
  //   if (type == "mobile") {
  //     // ＠＠＠ Person
  //     phone_number->set_type(tutorial::Person::MOBILE);
  //   } else if (type == "home") {
  //     // ＠＠＠ Person
  //     phone_number->set_type(tutorial::Person::HOME);
  //   } else if (type == "work") {
  //     // ＠＠＠ Person
  //     phone_number->set_type(tutorial::Person::WORK);
  //   } else {
  //     cout << "Unknown phone type.  Using default." << endl;
  //   }
  // }

}
//*/

// Main function:  Reads the entire address book from a file,
//   adds one person based on user input, then writes it back out to the same
//   file.
int main(int argc, char* argv[]) {
  // Verify that the version of the library that we linked against is
  // compatible with the version of the headers we compiled against.
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  if (argc != 2) {
    cerr << "Usage:  " << argv[0] << " ADDRESS_BOOK_FILE" << endl;
    return -1;
  }
  // ＠＠＠ AddressBook
  //tutorial::AddressBook address_book;
   //GpsPoint gps_point;
   GpsPathTrackingInfo gps_path_tracking_info;
  {
    // Read the existing address book.
    fstream input(argv[1], ios::in | ios::binary);
    if (!input) {
      cout << argv[1] << ": File not found.  Creating a new file." << endl;
    // ＠＠＠ ParseFromIstream
    } else if (!gps_path_tracking_info.ParseFromIstream(&input)) { /*将 message 写入给定的 C++ 的 ostream*/
      cerr << "Failed to parse address book." << endl;
      return -1;
    }
  }

  // Add an address.
  PromptForAddress(gps_path_tracking_info.add_cur_gps_point());
  {
    // Write the new address book back to disk.
    fstream output(argv[1], ios::out | ios::trunc | ios::binary);
    // ＠＠＠ SerializeToOstream
    if (!gps_path_tracking_info.SerializeToOstream(&output)) {
      cerr << "Failed to write address book." << endl;
      return -1;
    }
  }
  cout << sizeof(gps_path_tracking_info) << endl;

#if 0
#endif
  // Optional:  Delete all global objects allocated by libprotobuf.
  // ＠＠＠ ShutdownProtobufLibrary
  google::protobuf::ShutdownProtobufLibrary();

  return 0;
}
