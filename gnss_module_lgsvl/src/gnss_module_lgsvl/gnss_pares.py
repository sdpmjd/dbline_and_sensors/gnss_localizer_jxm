#coding=utf-8

import  logging
from copy import  deepcopy
import math 

class GpsPares(object):

    '''
    华测 P3 解析
    '''

    def __init__(self):
        pass

        self.info = {}

    """
    #HEADINGA,ICOM2,0,63.0,FINE,2046,458706.000,243751,33,18;SOL_COMPUTED,NARROW_FLOAT,1.3035,89.6393,36.3885,0.0000,55.3611,97.5243,"999",20,12,12,8,3,00,0,f3*82c553f4
    $GPGGA,072448.00,3642.23085003,N,11708.94358450,E,5,13,1.1,44.6024,M,0.0000,M,03,0000*65
    $GNVTG,266.926,T,273.232,M,0.01083,N,0.02005,K,D*3F
    
    #HEADINGA,ICOM2,0,63.0,FINE,2046,458707.000,244751,30,18;SOL_COMPUTED,NARROW_FLOAT,1.3100,88.6880,36.9684,0.0000,53.7964,94.1368,"999",20,12,12,8,3,00,0,f3*8533fcb4
    $GPGGA,072449.00,3642.23079135,N,11708.94355209,E,5,13,1.1,44.5136,M,0.0000,M,03,0000*65
    $GNVTG,204.731,T,211.037,M,0.01866,N,0.03455,K,D*33
    
    #HEADINGA,ICOM2,0,63.0,FINE,2046,458708.000,245751,30,18;SOL_COMPUTED,NARROW_FLOAT,1.2919,86.4853,37.6880,0.0000,53.0232,71.6334,"999",20,13,13,8,3,00,0,f3*0e838fd0
    $GPGGA,072450.00,3642.23076672,N,11708.94356923,E,5,13,1.1,44.5337,M,0.0000,M,03,0000*65
    $GNVTG,175.653,T,181.959,M,0.01360,N,0.02519,K,D*3D
    
    #HEADINGA,ICOM2,0,63.0,FINE,2046,458709.000,246751,30,18;SOL_COMPUTED,NARROW_FLOAT,1.2809,88.1282 

    QQ02C,INSATT,V,062624.535,0.000,0.000,273.769,@17
    """

    def gga_parse(self,tmp_list):

            """
            $GPGGA

            例：$GPGGA,092204.999,4250.5589,S,14718.5084,E,1,04,24.4,19.7,M,,,,0000*1F

                字段0：$GPGGA，语句ID，表明该语句为Global Positioning System Fix Data（GGA）GPS定位信息
                字段1：UTC 时间，hhmmss.sss，时分秒格式
                字段2：纬度ddmm.mmmm，度分格式（前导位数不足则补0）
                字段3：纬度N（北纬）或S（南纬）
                字段4：经度dddmm.mmmm，度分格式（前导位数不足则补0）
                字段5：经度E（东经）或W（西经）
                字段6：GPS状态，0=未定位，1=单点定位，2=伪距/SBAS，3=无效PPS，4=RTK固定，5=RTK浮动，6=正在估算
                                  7=手动启动基准站，8=RTK宽巷解，9=伪距（诺瓦泰615）"
                字段7：正在使用的卫星数量（00 - 12）（前导位数不足则补0）
                字段8：HDOP水平精度因子（0.5 - 99.9）
                字段9：海拔高度（-9999.9 - 99999.9）
                字段10：地球椭球面相对大地水准面的高度
                字段11：差分时间（从最近一次接收到差分信号开始的秒数，如果不是差分定位将为空）
                字段12：差分站ID号0000 - 1023（前导位数不足则补0，如果不是差分定位将为空）
                字段13：校验值

            """
            try:
                "纬度: ddmm.mmmm，度分格式（前导位数不足则补0）"

                lat_gga = tmp_list[2]
                lat = 0.0
                if  lat_gga != "":
                    lat_gga_float = float( lat_gga)
                    lat_int = int(lat_gga_float/100)
                    lat = lat_int +  (lat_gga_float - (lat_int * 100))/60

                if  tmp_list[3] == "S":
                    lat *= -1;

                "经度 ddmm.mmmm，度分格式（前导位数不足则补0）"
                lon_gga = tmp_list[4]
                lon = 0.0
                if  lon_gga != "":
                    lon_gga_float = float( lon_gga)
                    lon_int = int(lon_gga_float/100)
                    lon = lon_int +  (lon_gga_float - (lon_int * 100))/60

                if  tmp_list[5] == "W":
                    lon *= -1;
                    pass

                gps_status = int("0" if tmp_list[6] == "" else tmp_list[6])
                cnt = int("0" if tmp_list[7] == "" else tmp_list[7])
                altitude = float("0" if tmp_list[9] == "" else tmp_list[9])

                info = {}
                info["lat"] = lat
                info["lon"] = lon
                info["gps_state"] = gps_status
                info["cnt"] = cnt
                info["altitude"] = altitude # 海拔
                # print(info["alt"])
                # logging.debug(" gga : %s" % info)
                return info
            except Exception as e:
                print(e)
                logging.exception(e)
                raise Exception("解析异常")

    def vtg_parse(self,tmp_list):
            """
            GPVTG
            Track Made Good and Ground Speed（VTG）地面速度信息

               0        1          2          3        4      5   6
            $GPVTG, 172.516,T,  155.295,M,  0.049,N, 0.090,K, D   *2B
            "$--VTG将取决于定位所用的卫星系统，
                $GP=只用GPS卫星
                $GL=只用GLONASS卫星
                $GN=使用所有卫星系统"
            字段0：$--VTG
            字段1：以真北为参考基准的地面航向（000~359度，前面的0也将被传输）
            字段2：以磁北为参考基准的地面航向（000~359度，前面的0也将被传输）
            字段3：地面速率（000.0~999.9节，前面的0也将被传输）
            字段4：地面速率（0000.0~1851.8公里/小时，前面的0也将被传输）
            字段5：模式指示（仅NMEA0183 3.00版本输出，A=自主定位，D=差分，E=估算，N=数据无效）
            字段6：校验值
            """
            try:
                info = {}
                info["azi_1"] = "0" if tmp_list[1] == "" else tmp_list[1]  # 真北航向角
                # info["azi_2"] = tmp_list[3]
                info["v"] = "0" if tmp_list[7] == "" else tmp_list[7]  # 速度 km/h
                # info["model"] = "N"if  tmp_list[9][0] ==  "" else tmp_list[9][0]
                logging.debug("vtg: %s" %info)
                return info
            except Exception as  e:
                logging.exception(e)
                raise Exception("解析异常")

    def headinga_parse(self,parse_arg):
        # logging.info(parse_arg)

        part_s = parse_arg.split(";")
        if part_s.__len__() < 2:
            logging.error("headinga_parse errr")
        tmp_list = part_s[1].split(',')
        """
        PTNL,AVR

            #HEADINGA,ICOM2,0,63.0,FINE,2046,458706.000,243751,33,18;SOL_COMPUTED,NARROW_FLOAT,1.3035,89.6393,36.3885,0.0000,55.3611,97.5243,"999",20,12,12,8,3,00,0,f3*82c553f4
            
            0 : #HEADINGA,ICOM2,0,63.0,FINE,2046,458706.000,243751,33,18;\
            1 : SOL_COMPUTED,
            2 : NARROW_FLOAT,
            3 : 1.3035,
            4 : 89.6393,    航向角
            5 : 36.3885,
            6 : 0.0000,
            7 : 55.3611,
            8 : 97.5243,
            9 : "999",
            10: 20,
            11: 12,
            12: 12,
            13: 8,
            14: 3,
            15: 00,
            16: 0,
            17: f3
            18: *82c553f4   校验值


        """
        try:
            info = {}
            info["azi_a"] = "0" if tmp_list[3] == "" else tmp_list[3]  # 偏航角
            # info["azi_a"] = "0" if tmp_list[5] == "" else tmp_list[5]  # 倾斜角
            cnt = tmp_list[11] #卫星个数
            info["cnt"] = "0" if cnt == "" else cnt
            if  info["azi_a"]  == "+0.0000":
                logging.error("azi_a == 0")
                info.pop("azi_a")
            logging.debug("headinga_parse : %s" % info)
            return info
        except Exception as e:
            logging.exception(e)
            raise Exception("解析异常")

    def avr_parse(self,tmp_list):
        """
        PTNL,AVR

            0          1          2          3      4        5    6  7    8     9   10  11  12
        $PTNL,AVR,  212405.20,  +52.1531,   Yaw,  -0.0806,  Tilt,  ,  , 12.575, 3, 1.4, 16  *39

            字段0：$PTNL,AVR
            字段1：UTC 时间，hhmmss.sss，时分秒格式
            字段2：航偏角，单位为“度”
            字段3：航偏
            字段4：倾斜角，单位为“度”
            字段5：倾斜
            字段6：横滚角，单位为“度”
            字段7：横滚
            字段8：基线长，单位为“米”
            字段9：定位状态，0=未定位，1=单点定位，2=浮动，3=固定，4=伪距差分
            字段10：PDOP精度因子（0.5 - 99.9）
            字段11：参与解算的卫星颗数
            字段12：校验值

        """
        try:
            info = {}
            info["azi_a"] = "0" if tmp_list[3] == "" else tmp_list[3]  # 偏航角
            # info["azi_a"] = "0" if tmp_list[5] == "" else tmp_list[5]  # 倾斜角
            info["model"] = "0" if tmp_list[10] else tmp_list[10]    # 定位状态  # 0=未定位，1=单点定位，2=浮动，3=固定，4=伪距差分
            cnt = tmp_list[12].split("*")[0] #卫星个数
            info["cnt"] = "0" if cnt == "" else cnt
            if  info["azi_a"]  == "+0.0000":
                logging.error("azi_a == 0")
                info.pop("azi_a")
            logging.debug("avr : %s" % info)
            return info
        except Exception as e:
            logging.exception(e)
            raise Exception("解析异常")

    def qq02c_parse(self,tmp_list): 
        # QQ02C,INSATT,V,062624.535,0.000,0.000,273.769,@17
        try:

            info = {}
            info['roll']    = float(tmp_list[4])
            info['pitch']   = float(tmp_list[5])
            if info['pitch'] < -180:
                info['pitch'] = 360.0 + info['pitch']
            # info['pitch'] = 0
            info['azi_a'] = float(tmp_list[6])
            _heading = float(tmp_list[6])

            # 逆时针旋转90度 -> 0度 朝东
            _heading -= 90      # Cube Town lgsvl 2929.05
            if _heading < 0: 
                _heading = 360 + _heading
            if _heading > 360:
                 _heading -= 360


            # // imu数值范围　转换为 [-180 , 180]
            _gnss_yaw = 0;
            if _heading > 180:
                _gnss_yaw = 360 - _heading
            else:
                _gnss_yaw = 0 - _heading

            info['yaw'] =_gnss_yaw 
            #  角度转换为弧度
            # RAD2DEG = 180.0/math.pi
            # info['yaw'] =_gnss_yaw/RAD2DEG 

            return info
        except Exception as e:
            raise Exception("解析异常")

    def parse( self,data ):
        if data =="" or data is None :
            return {}
        try:
            lat = None
            lon = None
            gps_state = None
            v = None
            cnt = None
            altitude = None

            vtg_info ={}
            avr_info={}
            headinga_info  = {}
            qq02c_info = {}
            # data = data.strip("\r\n")
            # pack_list = data.split("\r\n")
            data = data.strip("\n")
            pack_list = data.split("\n")

            if pack_list.__len__() > 3:
                logging.warning("package cnt > 3")
                pack_list = pack_list[0:3]
            # print(pack_list)
            for pack in pack_list:
                tmp_list = pack.split(",")
                if tmp_list[0] in  [ "$GPGGA","$GNGGA","$BDGGA","$GLGGA","$GAGGA"]:
                    ret = self.gga_parse(tmp_list)  # 解析经纬度
                    lat = ret.get("lat",None)
                    lon = ret.get("lon",None)
                    cnt = ret.get("cnt",None)
                    altitude = ret.get("altitude",None)
                    gps_state = ret.get("gps_state",None)
                
                    
                elif tmp_list[0] in [ "$GPVTG","$GNVTG","$BDVTG","$GLVTG","GAGGA"]:
                    vtg_info = self.vtg_parse(tmp_list)  # 取 速度
                    v =  vtg_info['v']
                elif tmp_list[0] ==  "$PTNL" and  tmp_list[1] == "AVR":  # 取 方向
                    avr_info =  self.avr_parse(tmp_list)
                elif tmp_list[0] in [ "#HEADINGA"]:
                    headinga_info = self.headinga_parse(pack)  # 取 方向
                elif tmp_list[0] in [ "QQ02C"]:
                     qq02c_info = self.qq02c_parse(tmp_list)
                    #  print(qq02c_info)
                else:
                    msg = "Can not find conmand ward :  %s " % tmp_list
                    logging.warning(msg)

            self.info["lat"] = lat  if lat else self.info.get("lat","0")
            self.info["lon"] = lon  if lon else self.info.get("lon","0")
            self.info["cnt"] = cnt  if cnt else self.info.get("cnt","0")
            self.info["h"] = altitude if altitude else self.info.get("h","0")
            self.info["v"]   = v  if v else self.info.get("v","0")       # 速度 km/h
            self.info["gps_state"]   = gps_state  if gps_state else self.info.get("gps_state","0")       # 速度 km/h
            if headinga_info == {}:
                if avr_info.get("azi_a"):
                    self.info["azi_a"] = avr_info.get("azi_a") # 航向角
            else:
                if headinga_info.get("azi_a"):
                    self.info["azi_a"] = headinga_info.get("azi_a") # 航向角

            if qq02c_info != {}:
                self.info.update(qq02c_info)

            return deepcopy(self.info)
        except Exception as e:
            logging.exception(e)
            return {}

class GpsPares_P2(object):

    ''' 华测 P2 解析 '''

    def __init__(self):
        self.info = {}

    def parse( self,msg ):

        info = {}
        for gpchc in msg.split('$'):
            data = gpchc.split(',')
            if data.__len__() < 24:
                continue

            try:
                info['azi_a'] = data[3]
                info['pitch'] = data[4]
                info['roll']  = data[5]

                info['gyro_x'] = data[6]
                info['gyro_y'] = data[7]
                info['gyro_z'] = data[8]
                info['acc_x' ] = data[9]
                info['acc_y' ] = data[10]
                info['acc_z' ] = data[11]

                info['lat'] = data[12]
                info['lon'] = data[13]

                info['v'] = float(data[18]) * 3.6
                info['cnt'] = data[19]
                info["gps_state"]  = int(data[21][0])
                # info["gps_state"]  =tmp_list[21][0:1]
                if data[21][1] == '0':
                    info["gps_state"] = int('101')
            except Exception as e:
                print(data)
                logging.exception(e)

        return info

class GpsPares_NewtonM2(object):
    pass
    """
    ['$GPFPD', '2117', '546559.380', '81.348', '1.461', 
      '0.295', '36.70388968', '117.14939980', '38.02', '0.005', 
      '0.001', '-0.002', '1.366', '22', '24', 
      '4B*2B']

    """
    def parse( self,msg ):
        info = {}
        tmp_list = msg.split(",")
        # print(tmp_list)
        try:
            if tmp_list[0] in ["$GPFPD"]:
                if tmp_list[3] == "":
                    print("GpsPares_NewtonM2  parse error !!!")
                    return {}

                info['azi_a'] = float(tmp_list[3])
                info['lat'] = float(tmp_list[6])
                info['lon'] = float(tmp_list[7])

                Ve = float(tmp_list[9]) # 东向速度 m/s
                Vn = float(tmp_list[10]) # 北向速度 m/s
                Vu = float(tmp_list[11]) # 天向速度 m/s 

                info['v'] = ((Ve**2 +Vn**2)**0.5)* 3.6
                info['cnt'] = int(tmp_list[13][0:2])
                info["gps_state"] = 0  
                if tmp_list[15][0:2] == "4B":
                    info["gps_state"] = 4  

                return info

            if tmp_list[0] in ["$GTIMU"]:
                info['acc_x'] = float(tmp_list[6]) * 9.8
                info['acc_y'] = float(tmp_list[7]) * 9.8
                info['acc_z'] = float(tmp_list[8]) * 9.8 - 9.8
                return info
        except Exception as e:
            print(e)
            return {}

import math
import struct
RAD2DEG = 180/math.pi

class GpsPares_NCOM(object):

    ''' NCOM 解析 '''


    def __init__(self):
        pass
        self.info = {}

    def parse( self,batch):

        if batch == None:
            return None

        info = {}
        if batch != None and batch.__len__() == 72:

            # logging.info("----")
            navigation_status = int.from_bytes(batch[21:22],byteorder='little',signed=False)
            # print(navigation_status)

            #batchB
            lat = struct.unpack('<d', batch[23:31])[0]
            lat *= RAD2DEG
            # print("lat  : ",lat*RAD2DEG)

            lon = struct.unpack('<d', batch[31:39])[0]
            lon *= RAD2DEG
            # print("lon  : ",lon)

            Altitude = struct.unpack('<f', batch[39:43])[0] #  高度
            # print("Altitude  : ",Altitude)

            # _barray = bytearray([0])
            north_velocity = int.from_bytes(batch[43:46],byteorder='little',signed=True)
            north_velocity *= 0.0001 #* 3.6
            # print("North Velocity  : ",north_velocity )

            east_velocity = int.from_bytes(batch[46:49],byteorder='little',signed=True)
            east_velocity *= 0.0001 #* 3.6
            # print("East Velocity  : ",east_velocity )

            down_velocity = int.from_bytes(batch[49:52],byteorder='little',signed=True)
            down_velocity *= 0.0001 #* 3.6
            # print("Down Velocity  : ",down_velocity )

            heading = int.from_bytes(batch[52:55],byteorder='little',signed=True)
            heading  = heading * 0.000001 * RAD2DEG
            if  heading < 0:
                heading += 360
            # print("heading  : ",heading)

            pitch = int.from_bytes(batch[55:58],byteorder='little',signed=True)
            pitch  = pitch * 0.000001 * RAD2DEG
            # print("pitch : ",pitch)

            roll = int.from_bytes(  batch[58:61],byteorder='little',signed=True)
            roll  = roll * 0.000001 * RAD2DEG
            # print("roll : ",roll)

            ###-------------------------------###
            info['azi_a'] = heading

            info['lat'] = lat
            info['lon'] = lon

            # info['v'] = (north_velocity**2 + east_velocity**2 + down_velocity**2 )**0.5
            info['v'] = ((north_velocity**2 + east_velocity**2)**0.5)* 3.6
            info['cnt'] = 0
            info["gps_state"]  = navigation_status

        return info

class GpsPares_INS(object):

    '''  北斗 INS 解析 '''
    '''
    AT_ITINS=<lat>,<lon>,<alt>,<height>,<utc>,<ns_vel>,<ew_vel>, 
    <gnd_spd>,<track_ angle>,<roll>,<pitch>,<heading>,<mag_heading>, 
    <roll_rate>,<pitch_rate>,<heading_rate><CR><LF>

    "AT_ITINS=36.7038,117.1486,52,38,2020.06.27-04:02:10,-1.3,-1.9,2.3,237,-0.3,0.2,233.9,0,-1.1,-0.7,-18.2"
    '''

    #5501596d00b4291d00acfffaff010001003a001300980f0000000000000000fe04ec015890e0156f87d345b9160000ffffffff00000000000000003a90e015e187d345f916000002000000250efaffffff0d7205213f0804bc14cdcd22b1305d1b0b05a701d0c2320000000cd721aa


    def __init__(self):
        pass
        self.info = {}

    def parse_ascii( self,msg):
        # "".strip
        try:
            msg = msg.lstrip("AT_ITINS=")
            # print(msg)
            # return None

            text_array = msg.split(',')
            if text_array.__len__() != 16 :
                print("error :text_array len = %s " % text_array.__len__())
                return {}

            info = {}
            info['lat'] = float(text_array[0])
            info['lon'] = float(text_array[1])
            info['v'] = float(text_array[7])
            info['azi_a'] = float(text_array[11])
            info['cnt'] = 0
            info["gps_state"] = 4
            return info.copy()

        except Exception as e:
            print(e)
            return {}

    def parse(self, msg):


        try:

            info = {}

            data = bytearray.fromhex(msg)[4:]

            info['lat_ins']  = int.from_bytes(data[30:34],byteorder='little',signed=True)/ 10000000.0
            info['lon_ins']  = int.from_bytes(data[34:38],byteorder='little',signed=True)/ 10000000.0

            info['lat_gnss']  = int.from_bytes(data[54:58],byteorder='little',signed=True)/ 10000000.0
            info['lon_gnss']  = int.from_bytes(data[58:62],byteorder='little',signed=True)/ 10000000.0

            info['lat'] = info['lat_ins'] 
            info['lon'] = info['lon_ins'] 

            # info['lat'] = info['lat_gnss'] 
            # info['lon'] = info['lon_gnss'] 


            info['East_speed']  = int.from_bytes(data[42:46],byteorder='little',signed=True) / 100.0# m/s
            info['North_speed']  = int.from_bytes(data[46:50],byteorder='little',signed=True)/100.0 # m/s
            info['Vertical_speed']  = int.from_bytes(data[50:54],byteorder='little',signed=True)/ 100.0 # m/s
            info['v'] = ( (info['East_speed']  ** 2 + info['North_speed'] ** 2) ** 0.5 ) * 3.6 # km/h

            gps_status = data[82] & 0x07
            solnSVs = data[84]
            # solnSVs


            info['Heading_GNSS'] = int.from_bytes(data[88:90],byteorder='little',signed=False) / 100.0
            # info['Pitch_GNSS'] = int.from_bytes(data[90:92],byteorder='little',signed=True) / 100.0 
            # info['Heading_STD_GNSS'] = int.from_bytes(data[92:94],byteorder='little',signed=False) 
            # info['Pitch_STD_GNSS'] = int.from_bytes(data[94:96],byteorder='little',signed=False) 

            info['heading'] = int.from_bytes(data[0:2],byteorder='little',signed=False) / 100.0
            info['pitch'] = int.from_bytes(data[2:4],byteorder='little',signed=True) / 100.0
            info['roll'] = int.from_bytes(data[4:6],byteorder='little',signed=True) / 100.0

            info['acc_x'] = int.from_bytes(data[12:14],byteorder='little',signed=True)/4000*9.8 
            info['acc_y'] = int.from_bytes(data[14:16],byteorder='little',signed=True)/4000*9.8
            info['acc_z'] = int.from_bytes(data[16:18],byteorder='little',signed=True) /4000*9.8 -9.8


            info['usw'] = int.from_bytes(data[24:26],byteorder='little',signed=False) 

            info['azi_a'] =info['heading']  
            # info['azi_a'] =info['Heading_GNSS']  

            # print("heading: %.2f  : Heading_GNSS %.2f " % (info['heading'],info['Heading_GNSS']))


            info['cnt'] = 0
            info['cnt'] = solnSVs
            if info['usw'] :
                print("error usw =  %s" %info['usw'] )

            # info["gps_state"] =gps_status
            GPS_STAUTS = [0,1,2,3,5,4,7,8,9,10] 
            info["gps_state"] =GPS_STAUTS[gps_status] 

            # print(info)
            return info.copy()
        except Exception as e:
            print(e)
            return {}



def log_init():
    from utils.loginit import init_logger
    import platform
    if platform.system() == "Windows":
        init_logger("C:\\Users\\Administrator\Desktop\\gps.log")
    elif platform.system() == "Linux":
        init_logger("/tmp/gps.log")
    elif platform.system() == "Darwin":
        init_logger("/tmp/gps.log")
    else:
        init_logger()
def test_main():
    gps_pares = GpsPares_NewtonM2()
    msg = "$GPFPD,2117,546559.380,81.348,1.461,0.295,36.70388968,117.14939980,38.02,0.005,0.001,-0.002,1.366,22,24,4B*2B"
    ret = gps_pares.parse(msg)
    print(ret)

    pass
if __name__ == "__main__":
    test_main()

    pass


