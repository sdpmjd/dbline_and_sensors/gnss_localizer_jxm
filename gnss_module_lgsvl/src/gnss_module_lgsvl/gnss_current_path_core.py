#!/usr/bin/env python3
#coding=utf-8

import sys
import rospy
from std_msgs.msg import String
from pm_gnss_msgs.msg import GnssInfo
from pm_waypoint_follower_msgs.msg import WayPoints,WaypointInfo,CmdInfo

from geographiclib.geodesic import Geodesic

class GnssRecord():
    def __init__(self):
        self.geod = Geodesic.WGS84
        self.old_lon = None
        self.old_lat = None

        self.__max_limit = 30000
        self.__waypoints = WayPoints()

    def record_is_valid( self ,gnss_info,max_len = 0.02):
        if gnss_info.lat == 0 or gnss_info.lat ==0:
            return False

        cur_lat = gnss_info.lat
        cur_lon = gnss_info.lon

        if self.old_lat == None or self.old_lon == None:
            self.old_lat = cur_lat
            self.old_lon = cur_lon
            return False

        # if gnss_info.status  == GnssInfo.INIT:
        #     return False

        ret = self.geod.Inverse ( self.old_lat , self.old_lon , cur_lat , cur_lon )
        _len = ret['s12']

        self.old_lat = cur_lat
        self.old_lon = cur_lon
 

        if _len < max_len:
            return False
        else:
            return True

    def append(self,gnss_info):
        # if self.__array.__len__() < self.__max_limit:
        if self.__waypoints.waypoint_array.__len__() < self.__max_limit:

            waypoint_info = WaypointInfo()
            waypoint_info.lat = gnss_info.lat
            waypoint_info.lon = gnss_info.lon
            self.__waypoints.waypoint_array.append(waypoint_info)

    def get_waypoint_array(self):
        return self.__waypoints

    def reset(self,gnss_info):
        del self.__waypoints.waypoint_array[:]
        self.__waypoints.waypoint_array = []

class Node(object):
    def __init__(self):
        self.__info = {}
        rospy.init_node ( 'gnss_current_path_core' ,log_level=rospy.INFO ,anonymous=True)

        # rospy.loginfo("init")
        # gnss_type = rospy.get_param('~gnss_type', 'NMEA')
        # rospy.loginfo("gnss_type : %s" %gnss_type)

        rospy.Subscriber ( "/gnss_info", GnssInfo , self.Callback,queue_size=1)
        rospy.Subscriber ( "/cmd_info", CmdInfo , self.cmd_info_callback,queue_size=1)

        self.curren_waypoints_pub = rospy.Publisher ( '/historical_track_info' , WayPoints , queue_size=1 )

        self.__gnss_record = GnssRecord()

    def run(self):
        rospy.spin()

    def cmd_info_callback(self,msg):
        # if msg.cmd == CmdInfo.RUN:
        #     self.__gnss_record.reset()
        self.__gnss_record.reset()

    def Callback( self ,msg):
        try:
            gnss_info = msg

            if self.__gnss_record.record_is_valid(gnss_info,max_len=0.1):
                self.__gnss_record.append(gnss_info)
                # rospy.loginfo(self.__gnss_record.get_waypoint_array())
                # rospy.loginfo(type(self.__gnss_record.get_waypoint_array()))
                # rospy.loginfo(self.__gnss_record.get_waypoint_array().waypoint_array.__len__())
                self.curren_waypoints_pub.publish(self.__gnss_record.get_waypoint_array())
        except  Exception as e:
            print(e)
        finally:
            pass

def main():
    node = Node()
    node.run()

if __name__ == "__main__":
    main()
