
from PyQt5.QtNetwork import QUdpSocket,QHostAddress
from PyQt5.QtCore import QObject,QByteArray

# from gps_pb2 import GpsInfo
from  scripts.gps_path_tracking_pb2 import GpsInfo

class GpsMsgSender(QObject):
    def __init__(self):
        super(GpsMsgSender, self).__init__()
        self.udp_sender = QUdpSocket()

    def udp_send_msg(self,datagram):
        head_lengh  = datagram.__len__()
        head_bytes = int(head_lengh).to_bytes(4,byteorder="little",signed=False)
        msg = QByteArray(head_bytes)
        msg.append(datagram)
        # self.udp_sender.writeDatagram(msg.data(), QHostAddress.Broadcast, 45453)
        self.udp_sender.writeDatagram(msg.data(), QHostAddress.LocalHost, 45453)
        # print("send length %s" % head_lengh)

        # self.udp_sender.writeDatagram(msg.data(), QHostAddress.Broadcast, 45458)
        # or
        # msg = bytearray(head_bytes) + bytearray(datagram)
        # self.udp_sender.writeDatagram(msg, QHostAddress.Broadcast, 45453)

    def send_info( self,_info):
        gps_info = GpsInfo()
        gps_info.lat = _info['lat']
        gps_info.lon = _info['lon']
        gps_info.azi = _info['azimutha']
        gps_info.speed = _info['speed']
        gps_info.status = _info['gps_state']
        gps_info.SerializeToString()
        self.udp_send_msg(gps_info.SerializeToString())

if __name__ == "__main__":
    from PyQt5.QtCore import  QCoreApplication,QFile,QIODevice,QTextStream
    import sys
    app = QCoreApplication(sys.argv)

    # def get_gps_points(file = "../../test_L.gpsdata"):
    def get_gps_points(file = "test.gpsdata"):
        try:
            gps_map_point = [ ]
            qfile = QFile ( file )
            if not qfile.open ( QIODevice.ReadOnly ):
                raise("openf %s failed" % file)
            else:
                infile = QTextStream ( qfile )
                pre_point =  None
                while (not infile.atEnd ( )):
                    point_str = infile.readLine ( )
                    # N , E = point_str.split ( " " )
                    E, N = point_str.split ( " " )
                    point = {}
                    point['lat'] = float(N) #*100
                    point['lon'] = float(E) #*100
                    if pre_point == None:
                        point['azimutha'] = 0
                    else:
                        from geographiclib.geodesic import Geodesic
                        ret = Geodesic.WGS84.Inverse( pre_point['lat'], pre_point['lon'],
                                                      point['lat'],point['lon'])

                        angle = ret['azi1']
                        if  angle < 0:
                            angle = 360 + angle
                        point['azimutha']  = angle

                    # gps_map_point.append ( [float(N)*100,float(E)*100] )
                    gps_map_point.append ( point )
                    pre_point = point
                return gps_map_point
        except Exception as e:
               raise("get_gps_pionts filed")


    gps_msg_sender = GpsMsgSender()
    for point in get_gps_points():
        # print(point['azimutha'])

        """
        message GpsInfo {
           required double lat = 1;
           required double lon = 2;
           required double azi = 3;
           required double speed = 4;
           required uint32 status = 5;
        }
        """
        gps_info = GpsInfo()
        """
            ['ByteSize', 'Clear', 
            'ClearExtension', 'ClearField', 
            'CopyFrom', 'DESCRIPTOR', ' 'FindInitializationErrors',
             
              'IsInitialized', 'ListFields', 'MergeFrom', 'MergeFromString', 
               'FromString', 'ParseFromString',
               'SerializePartialToString', 'SerializeToString', 
               '__sizeof__',  
              'azi', 'lat', 'lon', 'speed', 'status']
        """

        # gps_info.lat = 36.117
        # gps_info.lon = 120.34567
        # gps_info.azi= 30
        # gps_info.speed = 1.2
        # gps_info.status = 4

        from random import randrange

        d_lat = randrange(-21,21)
        gps_info.lat = point["lat"]  + d_lat * 0.0000001 * 2
        d_lon = randrange(-21,21)
        gps_info.lon = point["lon"] + d_lon * 0.0000001 *2
        gps_info.azi= point['azimutha']
        gps_info.speed = 1.2
        gps_info.status = 4
        print(point)

        gps_info.SerializeToString()
        # print("dody size = " , gps_info.ByteSize())
        gps_msg_sender.udp_send_msg(gps_info.SerializeToString())
        import time
        time.sleep(0.002)
        # time.sleep(0.1)
        # app.exec()
