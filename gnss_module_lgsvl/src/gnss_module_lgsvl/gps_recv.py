#!/usr/bin/env python3
#coding=utf-8

import socket
import serial
import math
import  logging
# import datetime


class  GpsSerialRecv(object):
    def __init__(self,port, baud=9600, timeout = 1):
        super(GpsSerialRecv, self).__init__()

        self.serial = serial.Serial()
        self.serial.baudrate = baud
        self.serial.port = port
        self.serial.timeout = timeout

    def open( self ):
        if self.serial.isOpen():
            pass
        else:
            self.serial.open()
    def close( self ):
        if self.serial.isOpen():
            self.serial.close()

    def recv(self):
        try:

            # 111 bytes 
            data =self.serial.read_until(bytes([0xAA]))
            if data == None or data.__len__() == 0:
                return None
            if data[0]!=0x55:
                return None

            # data = self.serial.read(71)
            # ret = self.serial.read(1)

            return data

            # batch = bytearray(bytes([0xAA,0x55]) + data)
            # return batch
            # logging.exception(e)
            # return None


            #     # logging.debug('Recv : msg length = %s \n%s'%(reading_data.__len__(), reading_data))
            #     return  reading_data
            # else:
            #     # raise TimeoutError
            #     return ""
        except Exception as e:
            print(e)
            # logging.exception(e)
            return ""

    @classmethod
    def get_active_serial_ports(cls):
        import serial.tools.list_ports
        port_list =[]
        for port in serial.tools.list_ports.comports():
            port_list.append(port.device)
        return port_list


def log_init():
    from utils.loginit import init_logger
    import platform
    if platform.system() == "Windows":
        init_logger("./gps.log")
    elif platform.system() == "Linux":
        init_logger("/tmp/gps.log")
    elif platform.system() == "Darwin":
        init_logger("/tmp/gps.log")
    else:
        init_logger()


def test_serial():
    # from gps_pares import GpsPares
    import time
    import platform
    gps_serial_recv = None

    gps_serial_recv = GpsSerialRecv(port="/dev/ttyUSB0",baud=115200,timeout=1000)
    gps_serial_recv.open()

    # if platform.system() == "Windows":
    #     gps_serial_recv = GpsSerialRecv(port="COM2",baud=115200,timeout=1000)
    # elif platform.system() == "Linux":
    #     gps_serial_recv = GpsSerialRecv(port="/dev/ttyUSB0",baud=115200,timeout=1000)
    #     # gps_serial_recv = GpsSerialRecv(port="/dev/pts/8",baud=115200,timeout=1000)
    # elif platform.system() == "Darwin":
    #     gps_serial_recv = GpsSerialRecv(port="/dev/cu.usbserial",baud=115200,timeout=1000)
    # gps_serial_recv.get_active_serial_ports()
    # gps_pares = GpsPares()
    gps_serial_recv.recv()
    while True:
        # info = gps_serial_recv.recv()
        # info = gps_pares.parse(gps_serial_recv.recv())
        info = gps_serial_recv.recv()
        if info:
            # print(bytearray(info).__len__())
            print(bytearray(info).hex())
            # logging.info(bytearray(info).hex()))
        # time.sleep(1)

# def list_serial_port():
#     from gps_pares import GpsPares
#     # gps_serial_recv.get_active_serial_ports()
#     # gps_serial_recv = GpsSerialRecv(port="/dev/pts/8",baud=115200,timeout=1000)
#     gps_serial_recv = GpsSerialRecv(port="/dev/ttyUSB0",baud=115200,timeout=1000)


#     # GpsSerialRecv.get_active_serial_ports()
#     while True:
#         try:
#             info = gps_serial_recv.recv()
#             print(info)
#             # break
#             pass
#         except Exception as e:
#             pass
#             # logging.info()

if __name__ == '__main__':
    # log_init()
    # logging.debug("start")
    test_serial()
    # list_serial_port()

