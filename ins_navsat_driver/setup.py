from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    # packages=['libins_navsat_driver', 'libins_navsat_driver.nodes'],
    # package_dir={'': 'src'}
    package_dir={'': 'scripts'}
)

setup(**d)
