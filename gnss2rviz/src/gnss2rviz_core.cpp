/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vector>
#include <gnss2rviz/gnss2rviz.h>

namespace gnss2tfpose
{
Gnss2TFPoseNode::Gnss2TFPoseNode()
  : private_nh_("~")
{
  initForROS();
}

Gnss2TFPoseNode::~Gnss2TFPoseNode()
{}

void Gnss2TFPoseNode::initForROS()
{
  sub1_ = nh_.subscribe("/gnss/fix", 1, &Gnss2TFPoseNode::callbackFromGnssinfo, this);
  pub1_ = nh_.advertise<std_msgs::String>("gnss_rviz", 10);
}

void Gnss2TFPoseNode::callbackFromGnssinfo( const gps_common::GPSFix& fix)
{
      // fix.status.status 
      // fix.latitude 
      // fix.longitude 
      // fix.altitude 
      // fix.track  // 航向角
      // fix.speed // 地面速度
      // fix.climb //垂直速度
      // fix.dip   // yaw
      // fix.pitch 
      // fix.roll  

    std_msgs::String gnss_str;
    string status  = "NO FIX";
    if (fix.status.status == fix.status.STATUS_FIX)
      status  = "FIX";
    gnss_str.data =  " 状态: " + status + " \n";
    gnss_str.data += " 纬度:  " + std::to_string(fix.latitude)  + " °\n";
    gnss_str.data += " 经度: " + std::to_string(fix.longitude) + " °\n";
    gnss_str.data += " 海拔: " + std::to_string(fix.altitude) + " m\n";
    gnss_str.data += " 航向: " + std::to_string(fix.track) + " °\n";
    gnss_str.data += " 速度: " + std::to_string(fix.speed) + " m/s\n";
    // gnss_str.data += " roll : 10 \n";
    // gnss_str.data += " pitch: 11 \n";
    // gnss_str.data += " yaw  : 12 \n";
    pub1_.publish(gnss_str);
}


void Gnss2TFPoseNode::run()
{


  ros::spin();
}

}  // namespace waypoint_follower

