/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vector>
#include <gnss2tfpose/gnss2tfpose.h>

namespace gnss2tfpose
{
Gnss2TFPoseNode::Gnss2TFPoseNode()
  : private_nh_("~")
  , LOOP_RATE_( 10000/*30*/)
  ,MAP_FRAME_("gnss")
  ,GPS_FRAME_("gps")
{
  initForROS();
  // health_checker_ptr_ = std::make_shared<autoware_health_checker::HealthChecker>(nh_, private_nh_);
  // health_checker_ptr_->ENABLE();
    /*******************************************************
     *   设置原点坐标 
     * ****************************************************/
    Geocentric earth(Constants::WGS84_a(), Constants::WGS84_f());
    this->p_local = new LocalCartesian(earth);

    this->p_geod = new Geodesic(Constants::WGS84_a(), Constants::WGS84_f());


    if (false == get_gnss_ntd_origins(gnss_slam_pose_list)){
      // 设置原点坐标
      ROS_INFO( "Reset localcartesian : %.8f, %.8f, %.2f",localcartesian_lat0_,localcartesian_lon0_,localcartesian_h0_);
      this->p_local->Reset (localcartesian_lat0_,localcartesian_lon0_, localcartesian_h0_);
    }else{
      ROS_INFO( "========== Reset localcartesian yaml list =========");
      for (auto gnp : gnss_slam_pose_list){
        ROS_INFO( "localcartesian List  : [ lat %.8f, lon %.8f, altitude %.2f ] [ %.2f , %.2f , %.2f] ",
                  gnp.gnss_fix.latitude,gnp.gnss_fix.longitude,gnp.gnss_fix.altitude,
                  pose_ndt_gnss.pose.position.x, pose_ndt_gnss.pose.position.y, pose_ndt_gnss.pose.position.z);
      }
      ROS_INFO( "==================================================");
    }
    p_listener = new tf::TransformListener(ros::Duration(10));

}

Gnss2TFPoseNode::~Gnss2TFPoseNode()
{}

void Gnss2TFPoseNode::initForROS()
{
  // ros parameter settings
  // private_nh_.param<std::string>( "gps_way_point_file", gps_way_point_file, ""); // gps 路点
  // private_nh_.param( "velocity_source", velocity_source_, 0);
  // private_nh_.param( "publishes_for_steering_robot", publishes_for_steering_robot_, false);
  // private_nh_.param( "minimum_lookahead_distance", minimum_lookahead_distance_, 6.0);
  // nh_.param("vehicle_info/wheel_base", wheel_base_, 2.7);

  //本地笛卡尔坐标原点值
  nh_.param( "localcartesian_lat0", localcartesian_lat0_, 36.70267185);
  nh_.param( "localcartesian_lon0", localcartesian_lon0_, 117.14692528);
  nh_.param( "localcartesian_h0", localcartesian_h0_, 0.0);
  nh_.param( "localcartesian_yaw", localcartesian_yaw_, 0.0);

  nh_.param ("tf_ndt_gnss/position/x", pose_ndt_gnss.pose.position.x ,0.0 );
  nh_.param ("tf_ndt_gnss/position/y", pose_ndt_gnss.pose.position.y ,0.0 );
  nh_.param ("tf_ndt_gnss/position/z", pose_ndt_gnss.pose.position.z ,0.0 );

  nh_.param ("tf_ndt_gnss/orientation/x", pose_ndt_gnss.pose.orientation.x ,0.0 );
  nh_.param ("tf_ndt_gnss/orientation/y", pose_ndt_gnss.pose.orientation.y ,0.0 );
  nh_.param ("tf_ndt_gnss/orientation/z", pose_ndt_gnss.pose.orientation.z ,0.0 );
  nh_.param ("tf_ndt_gnss/orientation/w", pose_ndt_gnss.pose.orientation.w ,0.0 );

  ROS_INFO_STREAM(pose_ndt_gnss);

  // 

  // setup subscriber
  sub1_ = nh_.subscribe("/gnss/fix", 1, &Gnss2TFPoseNode::callbackFromGnssinfo, this);
  pub1_ = nh_.advertise<geometry_msgs::PoseStamped>("gnss_pose", 10);
}

void Gnss2TFPoseNode::callbackFromGnssinfo( const gps_common::GPSFix& gnss_fix)
{

      if (gnss_slam_pose_list.size() > 0){
            GnssNdtPose gnp_min = gnss_slam_pose_list[0];
            double min_len = get_distance_by_two_gnss_points(gnss_fix.latitude,gnss_fix.longitude,
                                                            gnp_min.gnss_fix.latitude, gnp_min.gnss_fix.longitude);
            for (auto gnp : gnss_slam_pose_list){
              double len = get_distance_by_two_gnss_points( gnss_fix.latitude,gnss_fix.longitude,
                                                            gnp.gnss_fix.latitude,gnp.gnss_fix.longitude);

              // ROS_INFO( "len %2.f", len);

              if (len < min_len){
                gnp_min = gnp;
                min_len  = len;

                // ROS_INFO( "min GnssNdtPose  :  min_len %2.f [ lat %.8f, lon %.8f, altitude %.2f ] [ %.2f , %.2f , %.2f] ",
                //           min_len, gnp.gnss_fix.latitude,gnp.gnss_fix.longitude,gnp.gnss_fix.altitude,
                //           gnp_min.ndt_pose_origin.pose.position.x, 
                //           gnp_min.ndt_pose_origin.pose.position.y, 
                //           gnp_min.ndt_pose_origin.pose.position.z);

              }
            }

            this->p_local->Reset (gnp_min.gnss_fix.latitude, gnp_min.gnss_fix.longitude, gnp_min.gnss_fix.altitude);

            localcartesian_yaw_ = gnp_min.gnss_fix.dip;

            pose_ndt_gnss.pose.position.x  = gnp_min.ndt_pose_origin.pose.position.x; 
            pose_ndt_gnss.pose.position.y  = gnp_min.ndt_pose_origin.pose.position.y; 
            pose_ndt_gnss.pose.position.z  = gnp_min.ndt_pose_origin.pose.position.z; 

            pose_ndt_gnss.pose.orientation.x = gnp_min.ndt_pose_origin.pose.orientation.x ;
            pose_ndt_gnss.pose.orientation.y = gnp_min.ndt_pose_origin.pose.orientation.y ;
            pose_ndt_gnss.pose.orientation.z = gnp_min.ndt_pose_origin.pose.orientation.z ;
            pose_ndt_gnss.pose.orientation.w = gnp_min.ndt_pose_origin.pose.orientation.w ;

      }

      // GPS 坐标 转换为 笛卡尔坐标系
      double x = 0,y = 0,z = 0;
      // fix.status.status 
      // fix.latitude 
      // fix.longitude 
      // fix.altitude 
      // fix.track  // 航向角
      // fix.speed // 地面速度
      // fix.climb //垂直速度
      // fix.dip   // yaw
      // fix.pitch 
      // fix.roll  

      p_local->Forward(gnss_fix.latitude, gnss_fix.longitude, gnss_fix.altitude, x, y, z);

      // geometry_msgs::Pose pose;
      pose_.position.x = x;
      pose_.position.y = y;
      pose_.position.z = z;

      // 发布 tf 坐标变换
      tf::Transform transform; //声明一个变量用来存储转换信息
      transform.setOrigin(tf::Vector3(x, y, z));  //坐标系的位移变换

      tf::Quaternion q; // 两个参考系之间的旋转变换
      yaw_ = gnss_fix.dip/RAD2DEG;
      pitch_ = gnss_fix.pitch/RAD2DEG;
      roll_ = gnss_fix.roll/RAD2DEG;

      /****************************************
       * publish TF  gps
      ****************************************/
      {
        tf::Transform transform;
        transform.setOrigin(tf::Vector3(pose_.position.x, pose_.position.y, pose_.position.z));
        tf::Quaternion quaternion;
        quaternion.setRPY(roll_, pitch_, yaw_ );
        transform.setRotation(quaternion);
        // broadcaster.sendTransform(tf::StampedTransform(transform, ros::Time::now(), MAP_FRAME_, GPS_FRAME_));
        broadcaster.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "gnss", "gps"));
      }

      /**********************************************************
       * 发布 gnss 与 map 的变换
       *********************************************************/
      {
        tf::Transform transform;
        transform.setOrigin(tf::Vector3(pose_ndt_gnss.pose.position.x, pose_ndt_gnss.pose.position.y, pose_ndt_gnss.pose.position.z));

        tf::Quaternion quaternion;
        // http://docs.ros.org/en/diamondback/api/tf/html/c++/namespacetf.html
        tf::quaternionMsgToTF (pose_ndt_gnss.pose.orientation, quaternion); // tf::quaternionTFToMsg (quaternion, _orientation);

        // 补偿GPS坐标姿态偏移
        tf::Quaternion q;
        q.setRPY(0, 0, -localcartesian_yaw_);  
        quaternion *= q;

        transform.setRotation(quaternion);
        broadcaster.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "map", "gnss"));
      }
      // publishPoseStamped();
      publishPoseStamped2();
}

/************************************************
 * 
 *  http://wiki.ros.org/tf/Overview/Using%20Published%20Transforms
 * 
 * **********************************************/

void Gnss2TFPoseNode::publishPoseStamped()
{

    geometry_msgs::PointStamped gps_point;
    // 讲ＧＰＳ坐标变换到　map下
    gps_point.header.frame_id = "gnss"; //MAP_FRAME_;
    gps_point.header.stamp = ros::Time();

    //just an arbitrary point in space
    gps_point.point.x = pose_.position.x;
    gps_point.point.y = pose_.position.y;
    gps_point.point.z = pose_.position.z;

    geometry_msgs::PointStamped map_point;
    // map_point.header.frame_id = "map";
    // 将 gps 转换到 map 下
    p_listener->transformPoint("map", gps_point, map_point);

    // double lat, lon, h;
    pose_.position.x = map_point.point.x; 
    pose_.position.y = map_point.point.y; 
    pose_.position.z = map_point.point.z; 

    geometry_msgs::PoseStamped pose;
    pose.header.frame_id = "map";
    pose.header.stamp = ros::Time::now();
    pose.pose.position.x = pose_.position.x;
    pose.pose.position.y = pose_.position.y;
    pose.pose.position.z = pose_.position.z;
  #if 0
    // pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(roll_, pitch_, yaw_ + 1.2145745625488966 );
  #else

    geometry_msgs::QuaternionStamped gps_quaternionstamped,map_quaternionstamped;
    geometry_msgs::Quaternion _orientation;

    tf::Quaternion quaternion;
    quaternion.setRPY(roll_, pitch_, yaw_ );
    tf::quaternionTFToMsg (quaternion, _orientation);

    gps_quaternionstamped.header.frame_id = "gnss"; //MAP_FRAME_;
    gps_quaternionstamped.quaternion=_orientation;
    p_listener->transformQuaternion("map", gps_quaternionstamped, map_quaternionstamped);

    pose.pose.orientation = map_quaternionstamped.quaternion;
  #endif 
    pub1_.publish(pose);
}
void Gnss2TFPoseNode::publishPoseStamped2()
{

  geometry_msgs::PoseStamped gps_posestamped,map_posestamped;
  gps_posestamped.header.frame_id = "gnss"; //MAP_FRAME_;
  gps_posestamped.header.stamp = ros::Time();
  gps_posestamped.pose.position.x = pose_.position.x;
  gps_posestamped.pose.position.y = pose_.position.y;
  gps_posestamped.pose.position.z = pose_.position.z;

#if 0
  geometry_msgs::Quaternion _orientation;
  tf::Quaternion quaternion;
  quaternion.setRPY(roll_, pitch_, yaw_ );
  tf::quaternionTFToMsg (quaternion, _orientation);
  gps_posestamped.pose.orientation = _orientation;
#else
  gps_posestamped.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(roll_, pitch_, yaw_);
#endif

  // 将gnss 下的坐标　转换到 map 下
  p_listener->transformPose("map", gps_posestamped, map_posestamped);

  pub1_.publish(map_posestamped);
}

  // #if 0
  // //************************************
  // tf::Pose tf_pose;
  // tf::poseMsgToTF(pose.pose, tf_pose);

  // tf::Quaternion q;
  // q.setRPY(0, 0, M_PI);  // radian [-0.039, 0.056, -3.137 ]
  // tf::Transform tf_trans(q, tf::Vector3(625.45280013, 88.3823944880, 27.85229));  // 基站为原点坐标系与地图坐标系原点的差

  // tf_pose = tf_trans.inverse() * tf_pose;
  // tf::poseTFToMsg(tf_pose, pose.pose);
  // //************************************
  // #endif


void Gnss2TFPoseNode::run()
{
  ros::Rate loop_rate(LOOP_RATE_);
  while (ros::ok()) {
    try {
        ros::spinOnce();
    }catch (const exception& e){
          cerr    << "Caught exception: " << e.what() << "\n";
    }
    loop_rate.sleep();
  }
}


double Gnss2TFPoseNode::get_distance_by_two_gnss_points( double lat1,double lon1, double lat2,double lon2)
{
	try {
		double s12;
		this->p_geod->Inverse(lat1, lon1, lat2, lon2, s12);
		return  s12;
	}catch(const exception& e){
		cerr << "Caught exception: " << e.what() << "\n";
		return 999999;
	}
}


bool Gnss2TFPoseNode::get_gnss_ntd_origins( std::vector<GnssNdtPose> &gnssNdtPoseList)
{
    Json::Reader reader;
    Json::Value root;

    std::string json_file = "/home/autoware/shared_dir/gnss_ndt_pose.json";
    std::ifstream in(json_file);

    if(!in.is_open()) {
        // cout << "open error\n" << endl;
        ROS_ERROR("get_gnss_ntd_origins: reader parse file failed ,  %s", json_file.c_str());
        return false;
    }

    if(reader.parse(in, root)) {
        // int node_size = root["poses"].size();
         for(auto const & node : root["poses"]) {
            GnssNdtPose gnp;

            { // gnss
              std::string text = node["gnss"]["lat"].asString();
              gnp.gnss_fix.latitude = static_cast<double>(std::stod(text.c_str()));

              text = node["gnss"]["lon"].asString();
              gnp.gnss_fix.longitude = static_cast<double>(std::stod(text.c_str()));

              text = node["gnss"]["altitude"].asString();
              gnp.gnss_fix.altitude = static_cast<double>(std::stod(text.c_str()));

              text = node["gnss"]["yaw"].asString();
              gnp.gnss_fix.dip = static_cast<double>(std::stod(text.c_str()));
            }

            { // slam
                // position
                std::string text = node["slam"]["position"]["x"].asString();
                gnp.ndt_pose_origin.pose.position.x = static_cast<double>(std::stod(text.c_str()));

                text = node["slam"]["position"]["y"].asString();
                gnp.ndt_pose_origin.pose.position.y = static_cast<double>(std::stod(text.c_str()));

                text = node["slam"]["position"]["z"].asString();
                gnp.ndt_pose_origin.pose.position.z = static_cast<double>(std::stod(text.c_str()));

                // orientation
                text = node["slam"]["orientation"]["w"].asString();
                gnp.ndt_pose_origin.pose.orientation.w = static_cast<double>(std::stod(text.c_str()));

                text = node["slam"]["orientation"]["x"].asString();
                gnp.ndt_pose_origin.pose.orientation.x = static_cast<double>(std::stod(text.c_str()));

                text = node["slam"]["orientation"]["y"].asString();
                gnp.ndt_pose_origin.pose.orientation.y = static_cast<double>(std::stod(text.c_str()));

                text = node["slam"]["orientation"]["z"].asString();
                gnp.ndt_pose_origin.pose.orientation.z = static_cast<double>(std::stod(text.c_str()));
            }
            gnssNdtPoseList.push_back(gnp);
        }
        in.close();
        return true;
    } else {
        // std::cout << "reader parse file failed" <<endl;
        ROS_ERROR("get_gnss_ntd_origins: reader parse file failed");
        return false;
    }
}


}  // namespace 