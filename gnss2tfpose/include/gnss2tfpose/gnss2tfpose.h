/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PURE_PURSUIT_PURE_PURSUIT_CORE_H
#define PURE_PURSUIT_PURE_PURSUIT_CORE_H

#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <gps_common/GPSFix.h>

// #include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseStamped.h>


#include <vector>
#include <memory>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/PointStamped.h> 
#include <tf/transform_listener.h>

#include <geometry_msgs/Pose.h>

// json
#include<fstream>
#include <iostream>
#include <json/json.h>


//  Geocentric
#include <GeographicLib/Geocentric.hpp>
#include <GeographicLib/Geodesic.hpp>
#include <GeographicLib/LocalCartesian.hpp>
using namespace GeographicLib;


#include <cmath>
#define RAD2DEG  (180.0/M_PI)

using namespace std;

namespace gnss2tfpose 
{

struct GnssNdtPose{
  gps_common::GPSFix         gnss_fix ;
  geometry_msgs::PoseStamped ndt_pose_origin;
};

enum class Mode : int32_t
{
  waypoint,
  dialog,
  unknown = -1,
};

template <class T>
typename std::underlying_type<T>::type enumToInteger(T t)
{
  return static_cast<typename std::underlying_type<T>::type>(t);
}


class Gnss2TFPoseNode

{
public:
  Gnss2TFPoseNode();
  ~Gnss2TFPoseNode();

  void run();
  // friend class PurePursuitNodeTestSuite;

private:
  // handle
  ros::NodeHandle nh_;
  ros::NodeHandle private_nh_;

  // publisher
  ros::Publisher pub1_, pub_1,pub_2,pub_3,pub_4;

  // subscriber
  ros::Subscriber sub1_, sub2_;

  // constant
  const int LOOP_RATE_;  // processing frequency
  const std::string MAP_FRAME_;  // processing frequency
  const std::string GPS_FRAME_;  // processing frequency
  tf::TransformListener *p_listener;
  
  //  Geocentric
  LocalCartesian * p_local;
  Geodesic *p_geod;

  tf::TransformBroadcaster broadcaster;

  geometry_msgs::Pose pose_;
  double roll_, pitch_, yaw_;

  geometry_msgs::PoseStamped pose_ndt_gnss; 

  // variables
  // bool is_linear_interpolation_, 
  //       publishes_for_steering_robot_, 
  //       add_virtual_end_waypoints_;

  // bool is_waypoint_set_, is_pose_set_, is_velocity_set_;
  // double current_linear_velocity_, command_linear_velocity_;
  // double wheel_base_;
  // int expand_size_;

  // int32_t velocity_source_;          // 0 = waypoint, 1 = Dialog
  // double const_lookahead_distance_;  // meter
  // double const_velocity_;            // km/h
  // double lookahead_distance_ratio_;
  // double minimum_lookahead_distance_;

  double localcartesian_lat0_; // 坐标原点值
  double localcartesian_lon0_;
  double localcartesian_h0_;
  double localcartesian_yaw_;


  std::vector<GnssNdtPose> gnss_slam_pose_list;

  // initializer
  void initForROS();


  // callbacks
  void callbackFromGnssinfo( const gps_common::GPSFix& gnss_fix);

  // bool get_gnss_ntd_origins(gps_common::GPSFix &gnss_fix, geometry_msgs::PoseStamped &ndt_pose_origin);

  bool get_gnss_ntd_origins(std::vector<GnssNdtPose> &gnssNdtPoseList);

  double get_distance_by_two_gnss_points( double lat1,double lon1, double lat2,double lon2);


  void publishPoseStamped();
  void publishPoseStamped2();
  // void publishTF();
};


}  // namespace waypoint_follower

#endif  // PURE_PURSUIT_PURE_PURSUIT_CORE_H
