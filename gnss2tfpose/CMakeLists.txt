cmake_minimum_required(VERSION 2.8.3)
project(gnss2tfpose)


find_package(
  catkin REQUIRED COMPONENTS
    geometry_msgs
    roscpp
    roslint
    rostest
    rosunit
    std_msgs
    tf
    tf2
)

include(FindPkgConfig)
pkg_check_modules(GEOGRAPHICLIB REQUIRED IMPORTED_TARGET geographiclib>=1.49)  # 检查所有相应的包

catkin_package(
  INCLUDE_DIRS include
)

include_directories(PkgConfig::GEOGRAPHICLIB)

include_directories(PkgConfig::JsonCpp)



add_compile_options(-std=c++11)
SET(CMAKE_CXX_FLAGS "-O2 -g -Wall ${CMAKE_CXX_FLAGS}")
set(ROSLINT_CPP_OPTS "--filter=-build/c++11")
roslint_cpp()

include_directories(
  include
  /usr/include/jsoncpp 
  ${catkin_INCLUDE_DIRS}
  #-ljsoncpp
)

add_executable(
  ${PROJECT_NAME}_node
  src/gnss2tfpose_node.cpp
  src/gnss2tfpose_core.cpp
  
)

add_dependencies(${PROJECT_NAME}_node ${catkin_EXPORTED_TARGETS}) # 检查依赖

target_link_libraries(${PROJECT_NAME}_node ${catkin_LIBRARIES})
target_link_libraries(${PROJECT_NAME}_node  PkgConfig::GEOGRAPHICLIB )

target_link_libraries(${PROJECT_NAME}_node  jsoncpp)




##############################################
#   INSTALL
##############################################

install(
  TARGETS ${PROJECT_NAME}_node
  #ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  #LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(
  DIRECTORY launch/
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/launch
)