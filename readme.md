# README

### topics

```
publish :
    /gnss/imu_raw    sensor_msgs/Imu
    /gnss/navsat_fix sensor_msgs/NavSatFix
    /gnss/fix		 gps_common/GPSFix
```

### Requirements

apt-get install python3-geographiclib


### A. Build Source

```bash

mkdir  catkin_ws

git clone  https://gitlab.com/sdpmjd/dbline_and_sensors/gnss_localizer_jxm.git catkin_ws/src 

cd catkin_ws

rosdep update

rosdep install -y --from-paths src --ignore-src --rosdistro $ROS_DISTRO

catkin_make

```

### B. Using the Debian Package

```
tar xzvf gnss_localizer_jxm_v1.0.0.tgz -C /tmp/

sudo dpkg -i  /tmp/*.deb
```

### Run

* With Carla

```sh
roslaunch carla_ros_bridge carla_ros_bridge_with_example_ego_vehicle.launch \
                host:=192.168.8.6 \
                town:=Town03 \
                vehicle_filter:=vehicle.toyota.prius  \
                spawn_point:="0.700499,-189.727951,0.2,0,0,-90"

roslaunch gnss_module  carla.launch use_gui:=True
```

* With LGSVL

```sh
roslaunch gnss_module lgsvl.launch use_gui:=True
```

* With NewtonM2

```sh
roslaunch gnss_module NewtonM2.launch port:=/dev/ttyUSB0 use_gui:=True
```

* With INS

```sh
roslaunch gnss_module INS.launch port:=/dev/ttyUSB0 use_gui:=True
```
* With NMEA

```sh
roslaunch gnss_module nmea.launch port:=/dev/ttyUSB0 use_gui:=True
```
