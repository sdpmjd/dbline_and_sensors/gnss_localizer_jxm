#!/usr/bin/env bash
#coding=utf-8

set -e  # 有错误就退出

#VERSION="123" # 版本号
#DATETIME=`date -d today +"%Y%m%d%H%M"`               # 时间

SELF_RELATIVE_DIR=`dirname $0`                       # 获取 脚本文件所在的相对路径
#SELF_ABSOLUTE_DIR=$(readlink -f "$SELF_RELATIVE_DIR")
SELF_ABSOLUTE_DIR=`readlink -f "$SELF_RELATIVE_DIR"` # 当前 脚本文件，所在的绝对路径

cd  ${SELF_ABSOLUTE_DIR}

tar xzvf gnss_localizer_jxm.tgz -C /tmp/

echo "deb [trusted=yes] file:///tmp/gnss_localizer_jxm /" > /etc/apt/sources.list.d/gnss_localizer_jxm-latest.list 
cat  /etc/apt/sources.list.dgnss_localizer_jxm-latest.list

# python3 -m pip install PyQtChart==5.10.1
python3 -m pip install PyQt5==5.10.1
python3 -m pip install geographiclib==1.49
python3 -m pip install pyserial==3.4
python3 -m pip install rospkg==1.2.3
# python3 -m pip install simple-pid==0.2.4
# python3 -m pip install cantools==34.0.0

sudo apt-get install -y --allow-downgrades libgeographic-dev=1.49-2

sudo apt-get install --reinstall -y --allow-downgrades \
                                    ros-melodic-nmea-msgs \
                                    ros-melodic-nmea-navsat-driver 
