# Autoware 使用 GPS定位 


* Depends

```sh
sudo python3 -m pip install PyQt5==5.10.1
sudo python3 -m pip install geographiclib==1.49
sudo python3 -m pip install pyserial==3.4
sudo python3 -m pip install rospkg==1.2.3

sudo apt-get install -y --allow-downgrades libgeographic-dev=1.49-2

sudo apt-get install --reinstall -y --allow-downgrades \
                                    ros-melodic-nmea-msgs \
                                    ros-melodic-nmea-navsat-driver 
```

* INSTALL

```sh
tar xzvf gnss_localizer_jxm.tgz -C /tmp/

cd /tmp/gnss_localizer_jxm

sudo dpkg -i *.deb
```

* Start
    * 启动 GNSS 客户端

    ```sh
    roslaunch gnss_module_gui start_gui.launch   # 启动 GNSS 客户端
    ## 勾选 Simulation 启动LGSVL连接（ 也可以，在 autoware 里面启动）

    #　查看输出
    rostopic echo /gnss/fix
    ```

    * 启动 GNSS 坐标投影

    ```sh

    # 按照视频，标定GPS和激光雷达的坐标系，标定完毕后（只需标定一次），启动 gnss2tfpose 。
    roslaunch gnss2tfpose start.launch

    #　查看输出
    rostopic echo /gnss_pose 
    ```