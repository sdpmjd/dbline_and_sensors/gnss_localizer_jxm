#!/usr/bin/env bash
#coding=utf-8

###########################################
#  更新源
###########################################

#VERSION="123" # 版本号
#DATETIME=`date -d today +"%Y%m%d%H%M"`               # 时间

SELF_RELATIVE_DIR=`dirname $0`                       # 获取 脚本文件所在的相对路径
#SELF_ABSOLUTE_DIR=$(readlink -f "$SELF_RELATIVE_DIR")
SELF_ABSOLUTE_DIR=`readlink -f "$SELF_RELATIVE_DIR"` # 当前 脚本文件，所在的绝对路径
cd $SELF_ABSOLUTE_DIR 

dpkg-scanpackages gnss_localizer_jxm /dev/null | gzip -9c > gnss_localizer_jxm/Packages.gz

# dpkg-scanpackages extension /dev/null | gzip -9c >  extension/Packages.gz
