#!/usr/bin/env bash
#coding=utf-8

#VERSION="123" # 版本号
#DATETIME=`date -d today +"%Y%m%d%H%M"`               # 时间

SELF_RELATIVE_DIR=`dirname $0`                       # 获取 脚本文件所在的相对路径
#SELF_ABSOLUTE_DIR=$(readlink -f "$SELF_RELATIVE_DIR")
SELF_ABSOLUTE_DIR=`readlink -f "$SELF_RELATIVE_DIR"` # 当前 脚本文件，所在的绝对路径

WORK_SRC_DIR=${SELF_ABSOLUTE_DIR}/../../
DEB_CACHE_DIR=${SELF_ABSOLUTE_DIR}/../.cache

cd  ${WORK_SRC_DIR}
BUILD_DIR=${WORK_SRC_DIR} #/dbline_skoda
echo  $BUILD_DIR

'''
gnss_module  
gnss_module_driver  
gnss_module_gui  
ins_navsat_driver  
nmea_navsat
gnss2tfpose
'''

PACKAGES='''
gnss_module_gui  
gnss2rviz
'''

cd $BUILD_DIR
for  file in ${PACKAGES}
do
	echo "=================================================="
	echo "                  " $file
	echo "=================================================="
	cd  ${file}


	bloom-generate rosdebian --os-name ubuntu --os-version bionic --ros-distro melodic
	# 2. Create binary debian
	fakeroot debian/rules binary

    rm -r obj-x86_64-linux-gnu
    rm -r debian
	cd $BUILD_DIR
done

DEST_DIR=${DEB_CACHE_DIR}/gnss_localizer_jxm_1.2/
mkdir -p $DEST_DIR

cd $BUILD_DIR
#cp -rvf *.deb  $DEST_DIR
echo $DEST_DIR
mv -v *.deb  $DEST_DIR

bash ${DEB_CACHE_DIR}/debian_repository_update.sh

cd ${DEB_CACHE_DIR}
rm gnss_localizer_jxm_v1.2.0.tgz
tar czvf gnss_localizer_jxm_v1.2.0.tgz  gnss_localizer_jxm_1.2