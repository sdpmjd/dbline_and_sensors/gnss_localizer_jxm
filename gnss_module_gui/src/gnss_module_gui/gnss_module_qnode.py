#!/usr/bin/env python3

from PyQt5.QtCore import QThread,pyqtSignal,QTimer

import rospy
from std_msgs.msg import String
from sensor_msgs.msg import Imu
from gps_common.msg import GPSFix
from geometry_msgs.msg import PoseStamped

class QNode(QThread):
    trigger = pyqtSignal ( dict , name="qnode_info" )
    def __init__(self,argv):
        super(QNode, self).__init__()
        self.init_argv = argv
        self.__info = {}
        rospy.init_node ( 'gps_module_qnode' , anonymous=True)
        self.ndt_pose = PoseStamped()

        self._input_topic_imu_raw = rospy.get_param('~input_topic_imu_raw', "/gnss/imu_raw") 
        self._input_topic_fix = rospy.get_param('~input_topic_fix', '/gnss/fix')

        rospy.Subscriber ( self._input_topic_imu_raw, Imu , self.imu_callback,queue_size=1)
        rospy.Subscriber ( self._input_topic_fix, GPSFix , self.gpsfix_callback,queue_size=1)
        rospy.Subscriber ( 'ndt_pose', PoseStamped , self.ndt_pose_callback,queue_size=1)

        rospy.loginfo("init")

        self.timer = QTimer()
        self.timer.timeout.connect(self.qtimer_callback)
        self.timer.setInterval(100)
        self.timer.start()

    def run(self) -> None:
        rospy.spin()

    def qtimer_callback(self):
        self.trigger.emit(self.__info.copy())

    def imu_callback(self,imu):
        self.__info['acc_x']  =  imu.linear_acceleration.x
        self.__info['acc_y']  =  imu.linear_acceleration.y
        self.__info['acc_z']  =  imu.linear_acceleration.z

    def ndt_pose_callback(self,pose):
        self.ndt_pose = pose

    def gpsfix_callback(self,fix):
        self.__info['lat']       =  fix.latitude
        self.__info['lon']       =  fix.longitude
        self.__info['altitude']  =  fix.altitude
        self.__info['speed']     =  fix.speed * 3.6
        self.__info['gps_state'] =  fix.status.status
        self.__info['azimutha']  =  fix.track
        self.__info['yaw']       =  fix.dip
        self.__info['pitch']     =  fix.pitch
        self.__info['roll']      =  fix.roll


if __name__ == "__main__":
    from PyQt5.QtCore import QCoreApplication
    import sys
    core_app = QCoreApplication(sys.argv)
    thread = QNode(sys.argv)
    thread.start()
    core_app.exec()
